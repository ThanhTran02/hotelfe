export type UserType = {
  Name?: string;
  TypeGuestID?: string;
  Cmnd?: string;
  Address?: string;
};
// export type RoomType = {
//   room: string;
//   type: string;
//   price: string;
//   note: string;
// };
export type BillRoomType = {
  phong: string;
  soNgayThue: string;
  donGia: string;
  thanhTien: string;
};

interface Room {
  RoomID: Number;
  TypeRoomID: Number;
  Status: true;
  RoomType: {
    TypeRoomID: Number;
    Name: string;
    Description: string;
    PricePerDay: string;
    MaxGuests: number;
  };
}
interface RoomType {
  TypeRoomID: number;
  Name: string;
  Description: string;
  PricePerDay: number;
  MaxGuests: number;
}
