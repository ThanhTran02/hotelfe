import { apiInstance } from "@/constant/apiInstance";
import { NextApiResponse } from "next";

const api = apiInstance({
  baseURL: process.env.NEXT_PUBLIC_BE_URL,
});
export const quanLyDatPhongServices = {
  createBooking: async (data: any) =>
    api.post<NextApiResponse<any>>(`./booking`, data),
  getBooking: async () => api.get<NextApiResponse<any>>(`./booking`),
  getBookingByID: async (id: any) =>
    api.get<NextApiResponse<any>>(`./booking/${id}`),
  getBookingByUserID: async (id: any) =>
    api.get<NextApiResponse<any>>(`./booking/search-by-id/${id}`),
  getBookingByUserName: async (name: any) =>
    api.get<NextApiResponse<any>>(`./booking/search?name=${name}`),
  updateBookingByUserID: async (id: any, data: any) =>
    api.put<NextApiResponse<any>>(`./booking/${id}`, data),
};
