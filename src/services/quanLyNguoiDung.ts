import { apiInstance } from "@/constant/apiInstance";
import { NextApiResponse } from "next";

const api = apiInstance({
  baseURL: process.env.NEXT_PUBLIC_BE_URL,
});
export const quanLyNguoiDungServices = {
  getAllGuest: async () => api.get<NextApiResponse<any>>(`./guest`),
  creatNewGuest: async (data: any) =>
    api.post<NextApiResponse<any>>(`./guest`, data),
  updateInfoGuest: async (id: any, data: any) =>
    api.put<NextApiResponse<any>>(`./guest/${id}`, data),
  findGuestfByID: async (id: any) =>
    api.get<NextApiResponse<any>>(`./guest/${id}`),
  findGuestfByName: async (name: any) =>
    api.get<NextApiResponse<any>>(`./guest/search/${name}`),
  deleteGuest: async (id: any) =>
    api.delete<NextApiResponse<any>>(`./guest/${id}`),
};
