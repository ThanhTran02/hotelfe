import { apiInstance } from "@/constant/apiInstance";
import { RoomType } from "@/types";
import { NextApiResponse } from "next";

const api = apiInstance({
  baseURL: process.env.NEXT_PUBLIC_BE_URL,
});
export const quanLyLoaiPhongServices = {
  getTypeRoom: async () => api.get<NextApiResponse<any>>(`./room-type`),
  createRoomType: async (data: any) =>
    api.post<NextApiResponse<any>>(`./room-type`, data),
  updateRoomType: async (id: number, data: any) =>
    api.put<NextApiResponse<any>>(`./room-type/${id}`, data),
};
