import { apiInstance } from "@/constant/apiInstance";
import { NextApiResponse } from "next";

const api = apiInstance({
  baseURL: process.env.NEXT_PUBLIC_BE_URL,
});
export const quanLyRoomServices = {
  getRoom: async () => api.get<NextApiResponse<any>>(`./room`),
  getIDRoom: async () => api.get<NextApiResponse<any>>(`./room/all-id-room`),
  getRoomByID: async (id: any) => api.get<NextApiResponse<any>>(`./room/${id}`),
  searchRoom: async (id: any) =>
    api.get<NextApiResponse<any>>(`./room/search?id=${id}`),
  createRoom: async (data: any) =>
    api.post<NextApiResponse<any>>(`./room`, data),
  updateRoom: async (id: any, data: any) =>
    api.put<NextApiResponse<any>>(`./room/${id}`, data),
};
