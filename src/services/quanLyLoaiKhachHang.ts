import { apiInstance } from "@/constant/apiInstance";
import { NextApiResponse } from "next";

const api = apiInstance({
  baseURL: process.env.NEXT_PUBLIC_BE_URL,
});
export const quanLyLoaiKhachHangServices = {
  getAllGuestType: async () => api.get<NextApiResponse<any>>(`./guest-type`),
};
