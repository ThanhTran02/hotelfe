import { apiInstance } from "@/constant/apiInstance";
import { NextApiResponse } from "next";

const api = apiInstance({
  baseURL: process.env.NEXT_PUBLIC_BE_URL,
});
export const quanLyNhanVienServices = {
  getAllStaff: async () => api.get<NextApiResponse<any>>(`./staff`),
  creatNewStaff: async (data: any) =>
    api.post<NextApiResponse<any>>(`./staff`, data),
  findStaffByID: async (id: any) =>
    api.get<NextApiResponse<any>>(`./staff/${id}`),
  deleteStaff: async (id: any) =>
    api.delete<NextApiResponse<any>>(`./staff/${id}`),
  updateInfoStaff: async (id: any, data: any) =>
    api.put<NextApiResponse<any>>(`./staff/${id}`, data),
};
