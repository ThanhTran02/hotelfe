import { apiInstance } from "@/constant/apiInstance";
import { NextApiResponse } from "next";

const api = apiInstance({
  baseURL: process.env.NEXT_PUBLIC_BE_URL,
});
export const quanLyQuyDinhServices = {
  getRegulation: async () => api.get<NextApiResponse<any>>(`./regulation`),
  getRegulationByID: async (id: string) =>
    api.get<NextApiResponse<any>>(`./regulation/${id}`),
  updateRegulation: async (id: string, data: any) =>
    api.put<NextApiResponse<any>>(`./regulation/${id}`, data),
};
