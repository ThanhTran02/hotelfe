import { apiInstance } from "@/constant/apiInstance";
import { NextApiResponse } from "next";

const api = apiInstance({
  baseURL: process.env.NEXT_PUBLIC_BE_URL,
});
export const quanLyBaoCaoServices = {
  getRevenueByYear: async (year: any) =>
    api.get<NextApiResponse<any>>(`./report/yearly-revenue?year=${year}`),
  getRevenueByRoomType: async (year: any) =>
    api.get<NextApiResponse<any>>(
      `./report/yearly-revenue/room-type?year=${year}`
    ),
  getRevenueByMonth: async (month: any, year: any) =>
    api.get<NextApiResponse<any>>(
      `./report/yearly-revenue/room-type-by-month?month=${month}&year=${year}`
    ),
};
