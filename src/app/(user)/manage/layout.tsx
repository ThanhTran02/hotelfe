"use client";
import React, { useState } from "react";
import {
  DesktopOutlined,
  PieChartOutlined,
  TeamOutlined,
  UserOutlined,
} from "@ant-design/icons";
import type { MenuProps } from "antd";
import { Avatar, Layout, Menu, Popover, theme } from "antd";
import { usePathname, useRouter } from "next/navigation";
import Link from "next/link";
import { TbMoneybag } from "react-icons/tb";
import { useAppDispatch } from "@/lib/hook";
import { userAction } from "@/lib/features/userSlice";
import { MdRoomPreferences } from "react-icons/md";
import { FcRules } from "react-icons/fc";
import { FaBuildingCircleCheck } from "react-icons/fa6";
const { Header, Content, Footer, Sider } = Layout;

type MenuItem = Required<MenuProps>["items"][number];

function getItem(
  label: React.ReactNode,
  key: string,
  icon?: React.ReactNode,
  children?: MenuItem[]
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem("Quản lý phòng", "/manage", <PieChartOutlined />),
  getItem("Thuê phòng", "/manage/hireroom", <DesktopOutlined />),
  getItem("Trả phòng", "/manage/checkout", <FaBuildingCircleCheck />),
  getItem("Quản lý loại phòng", "/manage/room", <MdRoomPreferences />),
  getItem("Quản lý nhân viên", "/manage/staff", <UserOutlined />),
  getItem("Quản lý người dùng", "/manage/user", <TeamOutlined />),
  getItem("Doanh Thu", "/manage/sales", <TbMoneybag />),
  getItem("Quy Định", "/manage/regulation", <FcRules />),
];

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const [collapsed, setCollapsed] = useState(false);
  const router = useRouter();
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  const pathname = usePathname();
  const dispatch = useAppDispatch();
  const content = (
    <div className="flex flex-col gap-5">
      <Link href={"/manage/profile"} className="text-black">
        Thông tin tài khoản
      </Link>
      <Link
        href={"/"}
        onClick={() => dispatch(userAction.logOut())}
        className="text-black"
      >
        Đăng xuất
      </Link>
    </div>
  );
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <div className="py-5" />
        <Menu
          theme="dark"
          defaultSelectedKeys={[pathname]}
          mode="inline"
          items={items}
          onClick={({ key }) => router.push(key)}
        />
      </Sider>
      <Layout>
        <Header
          style={{
            background: colorBgContainer,
          }}
          className="flex items-center justify-end px-5"
        >
          <Popover content={content} trigger="hover" placement="bottomRight">
            <Avatar icon={<UserOutlined />} className=" cursor-pointer" />
          </Popover>
        </Header>
        <Content style={{ margin: " 16px" }}>
          <div
            style={{
              padding: 24,
              minHeight: 360,
              background: colorBgContainer,
              borderRadius: borderRadiusLG,
            }}
          >
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Design ©{new Date().getFullYear()}
        </Footer>
      </Layout>
    </Layout>
  );
}
