"use client";
import { quanLyLoaiPhongServices } from "@/services";
import { quanLyRoomServices } from "@/services/quanLyRoom";
import { RoomType } from "@/types";
import {
  Button,
  Form,
  FormProps,
  Input,
  Modal,
  Select,
  SelectProps,
  Space,
  Table,
  TableProps,
} from "antd";
import { SearchProps } from "antd/es/input";
import { useEffect, useState } from "react";
import { CiEdit } from "react-icons/ci";
import { IoIosAddCircleOutline } from "react-icons/io";
import { toast } from "react-toastify";

interface DataType {
  key: string;
  name: string;
  age: number;
  address: string;
  tags: string[];
  stt?: number;
  phong?: string;
  loaiPhong?: string;
  donGia?: number;
  ghiChu?: string;
}

const { Search } = Input;
export default function Home() {
  const [form] = Form.useForm();

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalUpdateOpen, setIsModalUpdateOpen] = useState(false);
  const [options, setOptions] = useState<SelectProps["options"]>([]);
  const [dataTypeRoom, setDataTypeRoom] = useState<null | RoomType[]>();
  const [selectedTypeRoom, setSelectedTypeRoom] = useState<number>(1);
  const [dataTable, setDataTable] = useState<DataType[]>();
  const [formDataUpdate, setFormDataUpdate] = useState({
    RoomID: "",
    TypeRoomID: "",
    Status: "",
  });

  const onSearch: SearchProps["onSearch"] = async (value, _e, info) => {
    try {
      const { data } = await quanLyRoomServices.searchRoom(value);
      //@ts-ignore
      const newdataTable = data.content.map((item: any, index: any) => ({
        key: item.RoomID,
        stt: index + 1,
        room: item.RoomID,
        price: item.RoomType.PricePerDay,
        type: item.RoomType.Name,
        note: item.RoomType.Description,
        status: item.Status,
        maxGuests: item.RoomType.MaxGuests,
        roomtype: item.RoomType.TypeRoomID,
      }));
      setDataTable(newdataTable);
    } catch (error) {}
  };

  const handleTypeRoomChange = (value: string) => {
    setSelectedTypeRoom(Number(value));
  };

  const onFinish: FormProps<any>["onFinish"] = async (values) => {
    try {
      const res = await quanLyRoomServices.createRoom({
        ...values,
        RoomID: Number(values.RoomID),
      });
      toast.success("Thêm 1 Phòng thành công !");
      setSelectedTypeRoom(1);
      form.resetFields();
      handleCancel();
      fetchData();
    } catch (error) {
      toast.error("Vui lòng kiểm tra lại thông tin !");
      console.error("Error create new Staff:", error);
    }
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const fetchData = async () => {
    try {
      const { data } = await quanLyRoomServices.getRoom();
      //@ts-ignore
      const newdataTable = data.content.map((item: any, index: any) => ({
        key: item.RoomID,
        stt: index + 1,
        room: item.RoomID,
        price: item.RoomType.PricePerDay,
        type: item.RoomType.Name,
        note: item.RoomType.Description,
        status: item.Status,
        maxGuests: item.RoomType.MaxGuests,
        roomtype: item.RoomType.TypeRoomID,
      }));
      setDataTable(newdataTable);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const fetchTypeRoom = async () => {
    try {
      const { data } = await quanLyLoaiPhongServices.getTypeRoom();
      //@ts-ignore
      setDataTypeRoom(data.content);
      //@ts-ignore
      const newOptions = data.content.map((typeRoom: RoomType) => ({
        value: typeRoom.TypeRoomID,
        label: typeRoom.Name,
      }));
      setOptions(newOptions);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const handleCancelUpdate = () => {
    setIsModalUpdateOpen(false);
  };

  const showModallUpdate = (record: any) => {
    //@ts-ignore
    setFormDataUpdate({
      RoomID: record.room,
      TypeRoomID: record.roomtype,
      Status: record.status,
    });
    setIsModalUpdateOpen(true);
  };

  const onFinishUpdate = async () => {
    console.log("Success:", formDataUpdate);
    const { RoomID, ...newFormData } = formDataUpdate;
    try {
      const res = await quanLyRoomServices.updateRoom(RoomID, {
        ...newFormData,
        TypeRoomID: Number(newFormData.TypeRoomID),
        Status: newFormData.Status === "true",
      });
      toast.success("Cập nhật thông tin phòng thành công!");
      console.log(res);
      handleCancelUpdate();
      fetchData();
    } catch (error) {
      console.error("Error update room:", error);
      toast.error("Vui lòng kiểm tra lại thông tin !");
    }
  };

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    setFormDataUpdate({
      ...formDataUpdate,
      [name]: value,
    });
  };

  const columns: TableProps<DataType>["columns"] = [
    {
      title: "STT",
      key: "stt",
      dataIndex: "stt",
      align: "center",
    },
    {
      title: "Phòng",
      dataIndex: "room",
      key: "room",
      align: "center",
    },
    {
      title: "Loại Phòng",
      dataIndex: "type",
      key: "price",
      align: "center",
    },
    {
      title: "Đơn Giá",
      dataIndex: "price",
      key: "price",
      align: "center",
      render: (price: number) => (
        <span>
          {new Intl.NumberFormat("vi-VN", {
            style: "currency",
            currency: "VND",
          }).format(price)}
        </span>
      ),
    },
    {
      title: "Ghi chú",
      key: "note",
      dataIndex: "note",
      align: "center",
    },
    {
      title: "Tình Trạng",
      key: "status",
      dataIndex: "status",
      align: "center",
      render: (status: boolean) => (status ? "Chưa đặt" : "Đặt phòng"),
    },
    {
      title: "Sức Chứa",
      key: "maxGuests",
      dataIndex: "maxGuests",
      align: "center",
    },
    {
      title: "Chỉnh sửa",
      key: "action",
      align: "center",
      render: (_, record: DataType) => (
        <Space size="middle">
          <Button
            type="dashed"
            danger
            onClick={() => {
              showModallUpdate(record);
            }}
          >
            <CiEdit className="text-2xl" />
          </Button>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    fetchTypeRoom();
    fetchData();
  }, []);
  return (
    <>
      <div className="flex justify-between items-center mb-4">
        <Search
          placeholder="Tìm phòng"
          allowClear
          onSearch={onSearch}
          style={{ width: 400 }}
        />
        <Button>
          <div className="flex gap-2 items-center" onClick={showModal}>
            <IoIosAddCircleOutline />
            <span>Thêm Phòng</span>
          </div>
        </Button>
      </div>

      <Table columns={columns} dataSource={dataTable} />
      <Modal
        title="Sửa thông tin nhân viên"
        open={isModalUpdateOpen}
        onCancel={handleCancelUpdate}
        footer={false}
      >
        <form
          onSubmit={(e) => {
            e.preventDefault();
            onFinishUpdate();
          }}
        >
          <div className="flex items-start flex-col justify-center gap-3 ">
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">ID Phòng:</p>
              <input
                type="text"
                className=" cursor-not-allowed border col-span-2 bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                required
                value={formDataUpdate.RoomID}
                disabled
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">Loại Phòng</p>

              <select
                className="col-span-2 text-center bg-white border border-gray-300 text-gray-900  rounded-md  block w-full py-1"
                value={formDataUpdate.TypeRoomID}
                name="TypeRoomID"
                onChange={handleInputChange}
              >
                {options &&
                  options.map((item: any) => (
                    <option value={item.value} key={item.value}>
                      {item.label}
                    </option>
                  ))}
              </select>
            </div>
            <div className="grid grid-cols-3 gap-5 items-center  w-full">
              <p className="col-span-1 font-semibold ">Tình Trạng</p>
              <select
                className="col-span-2 text-center bg-white border border-gray-300 text-gray-900  rounded-md  block w-full py-1"
                value={formDataUpdate.Status}
                name="Status"
                onChange={handleInputChange}
              >
                <option value={"true"}>Chưa đặt</option>
                <option value={"false"}>Đã đặt</option>
              </select>
            </div>

            <button
              type="submit"
              className=" font-semibold border-black border-[1px] rounded-md px-2 py-1 w-full hover:bg-slate-200 duration-300 active:bg-slate-500 "
            >
              Cập nhật
            </button>
          </div>
        </form>
      </Modal>
      <Modal
        title="Thêm Phòng Mới"
        open={isModalOpen}
        onCancel={handleCancel}
        footer={false}
      >
        <Form
          form={form}
          name="basic"
          style={{ maxWidth: 600 }}
          labelCol={{ span: 8 }}
          onFinish={onFinish}
          initialValues={{ TypeRoomID: selectedTypeRoom }}
        >
          <Form.Item
            label="Tên Phòng"
            name="RoomID"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Loại phòng"
            name="TypeRoomID"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Select
              options={options}
              onChange={handleTypeRoomChange}
              //@ts-ignore
            />
          </Form.Item>
          <Form.Item label="Thông tin thêm">
            {dataTypeRoom && dataTypeRoom[selectedTypeRoom - 1].Description}
          </Form.Item>
          <Form.Item label="Giá mỗi đêm">
            {dataTypeRoom &&
              dataTypeRoom[selectedTypeRoom - 1].PricePerDay.toLocaleString()}
            ₫
          </Form.Item>
          <Form.Item label="Số hành khách tối đa">
            {dataTypeRoom && dataTypeRoom[selectedTypeRoom - 1].MaxGuests}
          </Form.Item>

          <Form.Item style={{ width: "100%" }}>
            <Button type="primary" htmlType="submit" style={{ width: "100%" }}>
              THÊM
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
}
