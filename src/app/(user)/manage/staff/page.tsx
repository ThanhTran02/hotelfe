"use client";
import { quanLyNhanVienServices } from "@/services/quanLyNhanVien";

import { SearchProps } from "antd/es/input";
import React, { useEffect, useState } from "react";
import { IoIosAddCircleOutline } from "react-icons/io";
import {
  Button,
  Form,
  type FormProps,
  Input,
  Modal,
  Table,
  Select,
  DatePicker,
  Space,
} from "antd";
import { toast } from "react-toastify";
import { CiEdit } from "react-icons/ci";
const { Search } = Input;

const page = () => {
  const [formData, setFormData] = useState({
    StaffID: "",
    Name: "",
    Position: "",
    Phone: "",
    Email: "",
  });

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const [searchStaff, setSearchStaff] = useState("");
  const [dataStaff, setStaffData] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalUpdateOpen, setIsModalUpdateOpen] = useState(false);

  const warning = (id: any) => {
    Modal.warning({
      title: "Bạn thật sự muốn xóa người dùng này",
      onOk: async () => {
        try {
          await quanLyNhanVienServices.deleteStaff(id);
          toast.success("Xóa nhân viên thành công!");
          fetchData();
        } catch (error: any) {
          console.error("Error deleting staff:", error);
          toast.error(error);
        }
      },
    });
  };
  //dữ liệu bảng
  const columns: any = [
    {
      title: "ID Nhân viên",
      dataIndex: "StaffID",
      key: "StaffID",
      align: "center",
    },
    {
      title: "Họ Tên",
      dataIndex: "Name",
      key: "Name",
      align: "center",
    },
    {
      title: "Vị trí",
      dataIndex: "Position",
      key: "Position",
      align: "center",
    },
    {
      title: "Ngày sinh",
      dataIndex: "DateOfBirth",
      key: "DateOfBirth",
      align: "center",
      render: (date: any) => new Date(date).toLocaleDateString(),
    },
    {
      title: "Số điện thoại",
      dataIndex: "Phone",
      key: "Phone",
      align: "center",
    },
    {
      title: "Email",
      dataIndex: "Email",
      key: "Email",
      align: "center",
    },
    {
      title: "Chỉnh sửa",
      key: "action",
      align: "center",
      //@ts-ignore
      render: (_, record) => (
        <Space size="middle">
          <Button
            type="dashed"
            danger
            onClick={() => {
              showModallUpdate(record);
            }}
          >
            <CiEdit className="text-2xl" />
          </Button>

          <Button type="primary" danger onClick={() => warning(record.StaffID)}>
            Delete
          </Button>
        </Space>
      ),
    },
  ];
  //hàm lấy dữ liệu
  const fetchData = async () => {
    try {
      const { data } = await quanLyNhanVienServices.findStaffByID(searchStaff);
      // @ts-ignore
      const newDataStaff = data.content.map((item) => ({
        ...item,
        key: item.StaffID,
      }));
      setStaffData(newDataStaff);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const onFinish: FormProps<any>["onFinish"] = async (values) => {
    console.log("Success:", values);
    try {
      const res = await quanLyNhanVienServices.creatNewStaff(values);
      toast.success("Thêm nhân viên thành công!");
      console.log(res);
      handleCancel();
      fetchData();
    } catch (error) {
      console.error("Error create new Staff:", error);
    }
  };

  const onFinishUpdate = async () => {
    console.log("Success:", formData);
    const { StaffID, ...newFormData } = formData;
    //@ts-ignore
    delete newFormData.key;
    try {
      const res = await quanLyNhanVienServices.updateInfoStaff(
        StaffID,
        newFormData
      );
      toast.success("Cập nhật thông tin nhân viên thành công!");
      console.log(res);
      handleCancelUpdate();
      fetchData();
    } catch (error) {
      console.error("Error create new Staff:", error);
    }
  };

  //modal thêm nhân viên
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  // modal sửa
  const handleCancelUpdate = () => {
    setIsModalUpdateOpen(false);
  };
  const showModallUpdate = (record: any) => {
    setIsModalUpdateOpen(true);
    setFormData(record);
  };

  //search
  const onSearch: SearchProps["onSearch"] = async (value, _e, info) => {
    setSearchStaff(value);
  };

  useEffect(() => {
    fetchData();
  }, [searchStaff]);

  return (
    <>
      <div className="flex justify-between items-center mb-4">
        <Search
          placeholder="Tìm Nhân Viên theo Mã Nhân Viên"
          allowClear
          onSearch={onSearch}
          style={{ width: 400 }}
        />
        <Button>
          <div className="flex gap-2 items-center" onClick={showModal}>
            <IoIosAddCircleOutline />
            <span>Thêm nhân viên</span>
          </div>
        </Button>
      </div>
      {dataStaff && <Table dataSource={dataStaff} columns={columns} />}
      <Modal
        title="Thêm nhân viên"
        open={isModalOpen}
        onCancel={handleCancel}
        footer={false}
      >
        <Form
          name="basic"
          style={{ maxWidth: 600 }}
          labelCol={{ span: 8 }}
          onFinish={onFinish}
        >
          <Form.Item
            label="Tên nhân viên"
            name="Name"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Vị trí"
            name="Position"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Select
              options={[
                { value: "Quản lý ", label: "Quản lý" },
                { value: "Nhân viên lễ tân", label: "Nhân viên lễ tân" },
                { value: "Nhân viên phục vụ", label: "Nhân viên phục vụ" },
                { value: "Kế toán", label: "Kế toán" },
                { value: "Bảo vệ", label: "Bảo vệ" },
                { value: "Lễ Tân", label: "Lễ Tân" },
              ]}
            />
          </Form.Item>
          <Form.Item
            label="Ngày sinh"
            name="DateOfBirth"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <DatePicker />
          </Form.Item>
          <Form.Item
            label="Số điện thoại liên hệ"
            name="Phone"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input type="number" />
          </Form.Item>
          <Form.Item
            label="Email"
            name="Email"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input type="email" />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Thêm nhân viên
            </Button>
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        title="Sửa thông tin nhân viên"
        open={isModalUpdateOpen}
        onCancel={handleCancelUpdate}
        footer={false}
      >
        <form
          onSubmit={(e) => {
            e.preventDefault();
            onFinishUpdate();
          }}
        >
          <div className="flex items-start flex-col justify-center gap-3 ">
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">ID Nhân viên:</p>
              <input
                type="text"
                className="border col-span-2 bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                required
                value={formData.StaffID}
                disabled
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">Họ Tên</p>
              <input
                type="text"
                value={formData.Name}
                className="border col-span-2  bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                required
                name="Name"
                onChange={handleInputChange}
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center  w-full">
              <p className="col-span-1 font-semibold ">Vị trí</p>
              <select
                className="col-span-2 text-center bg-white border border-gray-300 text-gray-900  rounded-md  block w-full py-1"
                value={formData.Position}
                name="Position"
                onChange={handleInputChange}
              >
                <option value="Quản lý">Quản lý</option>
                <option value="Nhân viên lễ tân">Nhân viên lễ tân</option>
                <option value="Nhân viên phục vụ">Nhân viên phục vụ</option>
                <option value="Kế toán">Kế toán</option>
                <option value="Bảo vệ">Bảo vệ</option>
                <option value="Lễ Tân">Lễ Tân</option>
              </select>
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold ">Số điện thoại</p>
              <input
                type="text"
                className="border col-span-2  bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                value={formData.Phone}
                name="Phone"
                onChange={handleInputChange}
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold ">Email</p>
              <input
                type="text"
                name="Email"
                className="border  col-span-2 bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                value={formData.Email}
                onChange={handleInputChange}
              />
            </div>
            <button
              type="submit"
              className=" font-semibold border-black border-[1px] rounded-md px-2 py-1 w-full hover:bg-slate-200 duration-300 active:bg-slate-500 "
            >
              Cập nhật
            </button>
          </div>
        </form>
      </Modal>
    </>
  );
};

export default page;
