"use client";
import React, { useEffect, useState } from "react";
import { GoogleOutlined, UserOutlined } from "@ant-design/icons";
import {
  Button,
  Form,
  type FormProps,
  Input,
  Modal,
  DatePicker,
  Select,
} from "antd";
import { jwtDecode } from "jwt-decode";
import { RiMapPinUserFill } from "react-icons/ri";
import { FaRegUser } from "react-icons/fa";
import { FaPhone, FaPhoneFlip } from "react-icons/fa6";
import { LiaBirthdayCakeSolid } from "react-icons/lia";
import { quanLyNhanVienServices } from "@/services";
import { toast } from "react-toastify";
interface User {
  DateOfBirth: string;
  Email: string;
  IsDeleted: boolean;
  Name: string;
  Password: string;
  Phone: string;
  Position: string;
  StaffID: number;
  exp: number;
  iat: number;
  role: string;
}

const profilePage = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const onFinish: FormProps<any>["onFinish"] = async (values) => {
    console.log("Success:", values);
    try {
      const { data } = await quanLyNhanVienServices.updateInfoStaff(
        inforUser?.StaffID,
        values
      );
      console.log(data);
      //@ts-ignore
      setInforUser(data.content);
      toast.success("Cập nhật thông tin cá nhân thành công !");
      setIsModalOpen(false);
    } catch (error) {
      console.log(error);

      toast.error("Vui lòng kiểm tra lại thông tin !");
    }
  };

  const [inforUser, setInforUser] = useState<User | null>();

  useEffect(() => {
    const accessToken = localStorage.getItem("token") || null;
    if (accessToken) setInforUser(jwtDecode(accessToken));
  }, []);
  console.log(inforUser);

  return (
    <>
      <div className="bg-gradient-to-r from-cyan-200 to-cyan-500 h-20"></div>
      <div>
        <div className="h-10 flex items-center justify-between px-5">
          <div className=" p-2 bg-white rounded-full -translate-y-1/3">
            <UserOutlined className="text-xl" />
          </div>

          <Button type="primary" onClick={showModal}>
            Chỉnh sửa thông tin cá nhân
          </Button>
        </div>
      </div>
      <div>
        <h2 className="text-xl font-semibold">Thông tin cá nhân</h2>

        <div className="content flex gap-10 w-1/2">
          <div className="grid grid-cols-2 w-full p-4">
            <div className="flex flex-col gap-5 ">
              <div className="flex gap-2 items-center">
                <FaRegUser />
                <p>Name:</p>
              </div>
              <div className="flex gap-2 items-center">
                <RiMapPinUserFill />
                <p>Vị trí:</p>
              </div>
              <div className="flex gap-2 items-center">
                <GoogleOutlined className="mr-2" />
                <p>Email:</p>
              </div>
              <div className="flex gap-2 items-center">
                <FaPhone />
                <p>Số điện thoai:</p>
              </div>
              <div className="flex gap-2 items-center">
                <LiaBirthdayCakeSolid />
                <p>Ngày sinh:</p>
              </div>
            </div>
            <div className="flex flex-col gap-5 ">
              <p>{inforUser?.Name}</p>
              <p>{inforUser?.Position}</p>
              <p>{inforUser?.Email}</p>
              <p>{inforUser?.Phone}</p>
              <p>{inforUser?.DateOfBirth.split("T")[0]}</p>
            </div>
          </div>
        </div>
      </div>
      <Modal
        title="Chỉnh sửa thông tin cá nhân"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={false}
      >
        <Form
          name="basic"
          style={{ maxWidth: 600 }}
          labelCol={{ span: 8 }}
          onFinish={onFinish}
          //@ts-ignore
          initialValues={inforUser}
        >
          <Form.Item
            label="Tên nhân viên"
            name="Name"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Vị trí"
            name="Position"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Select
              options={[
                { value: "Quản lý ", label: "Quản lý" },
                { value: "Nhân viên lễ tân", label: "Nhân viên lễ tân" },
                { value: "Nhân viên phục vụ", label: "Nhân viên phục vụ" },
                { value: "Kế toán", label: "Kế toán" },
                { value: "Bảo vệ", label: "Bảo vệ" },
                { value: "Lễ Tân", label: "Lễ Tân" },
              ]}
            />
          </Form.Item>
          {/* <Form.Item
            label="Ngày sinh"
            name="DateOfBirth"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <DatePicker />
          </Form.Item> */}
          <Form.Item
            label="Số điện thoại liên hệ"
            name="Phone"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input type="number" />
          </Form.Item>
          <Form.Item
            label="Email"
            name="Email"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input type="email" />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Chỉnh sửa thông tin
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default profilePage;
