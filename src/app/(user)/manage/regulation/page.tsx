"use client";
import { quanLyNhanVienServices } from "@/services/quanLyNhanVien";

import React, { useEffect, useState } from "react";
import { Button, Input, Modal, Table, Space } from "antd";
import { toast } from "react-toastify";
import { CiEdit } from "react-icons/ci";
import { quanLyQuyDinhServices } from "@/services";

const page = () => {
  const [formData, setFormData] = useState({
    RegulationID: "",
    Description: "",
    Value: "",
  });

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const [dataRegulation, setDataRegulation] = useState([]);
  const [isModalUpdateOpen, setIsModalUpdateOpen] = useState(false);

  //dữ liệu bảng
  const columns: any = [
    {
      title: "ID Quy Định",
      dataIndex: "RegulationID",
      key: "RegulationID",
      align: "center",
    },
    {
      title: "Thông Tin Chi Tiết",
      dataIndex: "Description",
      key: "Description",
      align: "center",
    },
    {
      title: "Giá trị",
      dataIndex: "Value",
      key: "Value",
      align: "center",
    },

    {
      title: "Chỉnh sửa",
      key: "action",
      align: "center",
      //@ts-ignore
      render: (_, record) => (
        <Space size="middle">
          <Button
            type="dashed"
            danger
            onClick={() => {
              showModallUpdate(record);
            }}
          >
            <CiEdit className="text-2xl" />
          </Button>
        </Space>
      ),
    },
  ];
  //hàm lấy dữ liệu
  const fetchData = async () => {
    try {
      const { data } = await quanLyQuyDinhServices.getRegulation();
      // @ts-ignore
      const newData = data.content.map((item) => ({
        ...item,
        key: item.RegulationID,
      }));
      setDataRegulation(newData);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const onFinishUpdate = async () => {
    console.log("Success:", formData);
    const { RegulationID, ...newFormData } = formData;
    //@ts-ignore
    delete newFormData.key;
    try {
      const res = await quanLyQuyDinhServices.updateRegulation(
        RegulationID,
        newFormData
      );
      toast.success("Cập nhật thông tin thành công!");
      console.log(res);
      handleCancelUpdate();
      fetchData();
    } catch (error) {
      console.error("Error :", error);
    }
  };

  // modal sửa
  const handleCancelUpdate = () => {
    setIsModalUpdateOpen(false);
  };
  const showModallUpdate = (record: any) => {
    setIsModalUpdateOpen(true);
    setFormData(record);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      {dataRegulation && (
        <Table dataSource={dataRegulation} columns={columns} />
      )}

      <Modal
        title="Sửa thông tin quy định"
        open={isModalUpdateOpen}
        onCancel={handleCancelUpdate}
        footer={false}
      >
        <form
          onSubmit={(e) => {
            e.preventDefault();
            onFinishUpdate();
          }}
        >
          <div className="flex items-start flex-col justify-center gap-3 ">
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">ID Quy Định:</p>
              <input
                type="text"
                className="border col-span-2 cursor-not-allowed bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                required
                value={formData.RegulationID}
                disabled
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">Thông Tin Chi Tiết</p>
              <input
                type="text"
                value={formData.Description}
                className="border col-span-2  bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                required
                name="Description"
                onChange={handleInputChange}
              />
            </div>

            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold "> Giá trị</p>
              <input
                type="text"
                name="Value"
                className="border  col-span-2 bg-white border-gray-300 text-gray-900  rounded-md block w-full py-1 text-center "
                value={formData.Value}
                onChange={handleInputChange}
              />
            </div>
            <button
              type="submit"
              className=" font-semibold border-black border-[1px] rounded-md px-2 py-1 w-full hover:bg-slate-200 duration-300 active:bg-slate-500 "
            >
              Cập nhật
            </button>
          </div>
        </form>
      </Modal>
    </>
  );
};

export default page;
