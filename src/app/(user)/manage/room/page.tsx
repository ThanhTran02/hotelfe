"use client";

import React, { useEffect, useState } from "react";

import { SearchProps } from "antd/es/input";
import { IoIosAddCircleOutline } from "react-icons/io";
import { Button, Form, type FormProps, Input, Modal, Table, Space } from "antd";
import { toast } from "react-toastify";
import { CiEdit } from "react-icons/ci";

import { quanLyLoaiPhongServices } from "@/services";
import { RoomType } from "@/types";

const page = () => {
  const [formData, setFormData] = useState<RoomType>({
    TypeRoomID: 0,
    Name: "",
    Description: "",
    PricePerDay: 0,
    MaxGuests: 0,
  });

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const [searchStaff, setSearchStaff] = useState("");
  const [dataRoomType, setDataRoomType] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalUpdateOpen, setIsModalUpdateOpen] = useState(false);
  const warning = (id: any) => {
    Modal.warning({
      title: "Bạn thật sự muốn xóa loại phòng này",
      onOk: async (id: any) => {
        try {
          //   await quanLyNhanVienServices.deleteStaff(id);
          //   toast.success("Xóa nhân viên thành công!");
          //   fetchData();
        } catch (error: any) {
          console.error("Error fetching data:", error);
          toast.error(error);
        }
      },
    });
  };
  //dữ liệu bảng

  const columns: any = [
    {
      title: "ID Phòng",
      dataIndex: "TypeRoomID",
      key: "TypeRoomID",
      align: "center",
    },
    {
      title: "Tên Loại Phòng",
      dataIndex: "Name",
      key: "Name",
      align: "center",
    },
    {
      title: "Thông Tin Thêm",
      dataIndex: "Description",
      key: "Position",
      align: "center",
    },
    {
      title: "Giá Tiền Mỗi Ngày",
      dataIndex: "PricePerDay",
      key: "PricePerDay",
      align: "center",
      render: (price: any) => (
        <span>
          {new Intl.NumberFormat("vi-VN", {
            style: "currency",
            currency: "VND",
          }).format(price)}
        </span>
      ),
    },
    {
      title: "Số Hành Khách Tối Đa",
      dataIndex: "MaxGuests",
      key: "MaxGuests",
      align: "center",
    },

    {
      title: "Chỉnh sửa",
      key: "action",
      align: "center",
      //@ts-ignore
      render: (_, record) => (
        <Space size="middle">
          <Button
            type="dashed"
            danger
            onClick={() => {
              showModallUpdate(record);
            }}
          >
            <CiEdit className="text-2xl" />
          </Button>

          {/* <Button type="primary" danger onClick={() => warning(record.StaffID)}>
            Delete
          </Button> */}
        </Space>
      ),
    },
  ];
  //hàm lấy dữ liệu
  const fetchData = async () => {
    try {
      const { data } = await quanLyLoaiPhongServices.getTypeRoom();
      // @ts-ignore
      const newData = data.content.map((item: RoomType) => ({
        ...item,
        key: item.TypeRoomID,
      }));
      setDataRoomType(newData);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const onFinish: FormProps<any>["onFinish"] = async (values) => {
    console.log("Success:", values);
    try {
      const res = await quanLyLoaiPhongServices.createRoomType({
        ...values,
        MaxGuests: Number(values.MaxGuests),
      });
      toast.success("Thêm 1 loại phòng thành công!");
      console.log(res);
      handleCancel();
      fetchData();
    } catch (error) {
      console.error("Error :", error);
      toast.error("Vui lòng kiểm tra lại các giá trị !");
    }
  };

  const onFinishUpdate = async () => {
    console.log("Success:", formData);
    const { TypeRoomID, ...newFormData } = formData;
    console.log(TypeRoomID);

    //@ts-ignore
    delete newFormData.key;
    try {
      const res = await quanLyLoaiPhongServices.updateRoomType(TypeRoomID, {
        ...newFormData,
        MaxGuests: Number(newFormData.MaxGuests),
      });
      toast.success("Cập nhật thông tin loại phòng thành công !");
      console.log(res);
      handleCancelUpdate();
      fetchData();
    } catch (error) {
      console.error("Error:", error);
      toast.error("Vui lòng kiểm tra lại các giá trị !");
    }
  };

  //modal thêm nhân viên
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  // modal sửa
  const handleCancelUpdate = () => {
    setIsModalUpdateOpen(false);
  };
  const showModallUpdate = (record: any) => {
    setIsModalUpdateOpen(true);
    setFormData(record);
  };

  //search
  const onSearch: SearchProps["onSearch"] = async (value, _e, info) => {
    setSearchStaff(value);
  };

  useEffect(() => {
    fetchData();
  }, [searchStaff]);

  return (
    <>
      <div className=" mb-4">
        <Button>
          <div className="flex gap-2 items-center" onClick={showModal}>
            <IoIosAddCircleOutline />
            <span>Thêm Loại Phòng</span>
          </div>
        </Button>
      </div>
      {dataRoomType && <Table dataSource={dataRoomType} columns={columns} />}
      <Modal
        title="Thêm Loại Phòng"
        open={isModalOpen}
        onCancel={handleCancel}
        footer={false}
      >
        <Form
          name="basic"
          style={{ maxWidth: 600 }}
          labelCol={{ span: 8 }}
          onFinish={onFinish}
        >
          <Form.Item
            label="Tên Loại Phòng"
            name="Name"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Thông tin thêm"
            name="Description"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Giá tiền mỗi đêm"
            name="PricePerDay"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input type="number"></Input>
          </Form.Item>
          <Form.Item
            label="Số hành khách tối đa"
            name="MaxGuests"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input type="number" />
          </Form.Item>

          <Form.Item style={{ width: "100%" }}>
            <Button type="primary" htmlType="submit" style={{ width: "100%" }}>
              Thêm Loại phòng
            </Button>
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        title="Sửa thông tin nhân viên"
        open={isModalUpdateOpen}
        onCancel={handleCancelUpdate}
        footer={false}
      >
        <form
          onSubmit={(e) => {
            e.preventDefault();
            onFinishUpdate();
          }}
        >
          <div className="flex items-start flex-col justify-center gap-3 ">
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">ID Phòng:</p>
              <input
                type="text"
                className="border col-span-2 cursor-not-allowed bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                required
                value={formData.TypeRoomID}
                disabled
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">Tên</p>
              <input
                type="text"
                value={formData.Name}
                className="border col-span-2  bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                required
                name="Name"
                onChange={handleInputChange}
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center  w-full">
              <p className="col-span-1 font-semibold ">Thông tin thêm</p>
              <input
                className="col-span-2 text-center bg-white border border-gray-300 text-gray-900  rounded-md  block w-full py-1"
                value={formData.Description}
                name="Description"
                onChange={handleInputChange}
              ></input>
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold ">Giá tiền mỗi đêm</p>
              <input
                type="number"
                min={0}
                className="border col-span-2  bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                value={formData.PricePerDay}
                name="PricePerDay"
                onChange={handleInputChange}
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold ">Số hành khách tối đa</p>
              <input
                type="number"
                name="MaxGuests"
                min={0}
                className="border  col-span-2 bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                value={formData.MaxGuests}
                onChange={handleInputChange}
              />
            </div>
            <button
              type="submit"
              className=" font-semibold border-black border-[1px] rounded-md px-2 py-1 w-full hover:bg-slate-200 duration-300 active:bg-slate-500 "
            >
              Cập nhật
            </button>
          </div>
        </form>
      </Modal>
    </>
  );
};

export default page;
