"use client";
import { SearchProps } from "antd/es/input";
import React, { useEffect, useState } from "react";
import { Button, Input, Modal, Table, Space, SelectProps } from "antd";
import { toast } from "react-toastify";
import { quanLyNguoiDungServices } from "@/services/quanLyNguoiDung";
import { CiEdit } from "react-icons/ci";
import { quanLyRoomServices } from "@/services/quanLyRoom";
import { quanLyLoaiKhachHangServices } from "@/services/quanLyLoaiKhachHang";
const { Search } = Input;

const page = () => {
  const [searchGuest, setSearchGuest] = useState("");
  const [dataGuest, setGuestData] = useState([]);

  const [formData, setFormData] = useState({
    GuestID: "",
    Name: "",
    RoomNumber: "",
    Address: "",
    Phone: "",
    Cmnd: "",
    TypeGuestID: "",
    Email: "",
  });

  const warning = (id: any) => {
    Modal.warning({
      title: "Bạn thật sự muốn xóa người dùng này",
      onOk: async () => {
        try {
          await quanLyNguoiDungServices.deleteGuest(id);
          toast.success("Xóa khách hàng thành công!");
          fetchData();
        } catch (error: any) {
          console.error("Error fetching data:", error);
          toast.error(error);
        }
      },
    });
  };

  const [optionsTypeGuest, setOptionsTypeGuest] = useState<
    SelectProps["options"]
  >([]);
  const [optionsRooms, setOptionsRoom] = useState<SelectProps["options"]>([]);
  const getRoomType = async () => {
    try {
      const { data } = await quanLyLoaiKhachHangServices.getAllGuestType();
      //@ts-ignore
      const newOptions = data.content.map((typeGuest) => ({
        value: typeGuest.GuestTypeID,
        label: typeGuest.Description,
      }));

      setOptionsTypeGuest(newOptions);
    } catch (error) {
      console.log(error);
    }
  };

  const columns: any = [
    {
      title: "ID Người dùng",
      dataIndex: "GuestID",
      key: "GuestID",
      align: "center",
    },
    {
      title: "Họ tên",
      dataIndex: "Name",
      key: "Name",
      align: "center",
    },

    {
      title: "Địa chỉ",
      dataIndex: "Address",
      key: "Address",
      align: "center",
    },
    {
      title: "Số điện thoại",
      dataIndex: "Phone",
      key: "Phone",
      align: "center",
    },
    {
      title: "Cmnd",
      dataIndex: "Cmnd",
      key: "Cmnd",
      align: "center",
    },
    {
      title: "Loại khách hàng",
      dataIndex: "GuestType",
      key: "GuestType",
      align: "center",
      //@ts-ignore
      render: (guestType) => guestType.Description,
    },
    {
      title: "Email",
      dataIndex: "Email",
      key: "Email",
      align: "center",
    },
    {
      title: "Thao tác",
      key: "action",
      align: "center",
      //@ts-ignore
      render: (_, record) => (
        <Space size="middle">
          <Button type="dashed" danger onClick={() => showModal(record)}>
            <CiEdit className="text-2xl" />
          </Button>

          <Button type="primary" danger onClick={() => warning(record.GuestID)}>
            Delete
          </Button>
        </Space>
      ),
    },
  ];

  const fetchData = async () => {
    try {
      const { data } = searchGuest
        ? await quanLyNguoiDungServices.findGuestfByName(searchGuest)
        : await quanLyNguoiDungServices.getAllGuest();

      // @ts-ignore
      const newDataGuest = data.content.map((item) => ({
        ...item,
        key: item.GuestID,
      }));

      setGuestData(newDataGuest);
    } catch (error) {
      console.error("Error fetching guest data:", error);
    }
  };

  const onFinishUpdate = async () => {
    console.log("Success:", formData);
    const { GuestID, ...newFormData } = formData;
    //@ts-ignore

    delete newFormData.key;
    //@ts-ignore
    delete newFormData.GuestType;

    try {
      const res = await quanLyNguoiDungServices.updateInfoGuest(
        GuestID,
        newFormData
      );
      toast.success("Cập nhật thông tin khách hàng thành công!");
      console.log(res);
      handleCancel();
      fetchData();
    } catch (error) {
      toast.error("Vui lòng kiểm tra lại thông tin!");
      console.error("Có lỗi khi cập nhật thông tin khách hàng:", error);
    }
  };

  //modal
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = (record: any) => {
    setIsModalOpen(true);
    setFormData(record);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  //search
  const onSearch: SearchProps["onSearch"] = async (value, _e, info) => {
    setSearchGuest(value);
    console.log(value);
  };

  const getIDRoom = async () => {
    try {
      const { data } = await quanLyRoomServices.getIDRoom();
      console.log(data);
      //@ts-ignore
      const newOptions = data.content.map((id_room) => ({
        value: parseInt(id_room.RoomID),
        label: id_room.RoomID,
      }));

      setOptionsRoom(newOptions);
    } catch (error) {
      console.log(error);
    }
  };

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    const newValue =
      name === "TypeGuestID" || name === "RoomNumber"
        ? parseInt(value, 10)
        : value.toString();
    setFormData({
      ...formData,
      [name]: newValue,
    });
  };
  useEffect(() => {
    fetchData();
    getRoomType();
    getIDRoom();
  }, [searchGuest]);

  return (
    <div>
      <div className=" mb-4">
        <Search
          placeholder="Tìm người dùng theo tên người dùng"
          allowClear
          onSearch={onSearch}
          style={{ width: 400 }}
        />
      </div>
      {dataGuest && <Table dataSource={dataGuest} columns={columns} />}

      <Modal
        title="Cập nhật thông tin người dùng"
        open={isModalOpen}
        onCancel={handleCancel}
        footer={false}
      >
        <form
          onSubmit={(e) => {
            e.preventDefault();
            onFinishUpdate();
          }}
        >
          <div className="flex items-start flex-col justify-center gap-3 ">
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">ID Người dùng:</p>
              <input
                type="text"
                className="border col-span-2 bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                required
                value={formData.GuestID}
                disabled
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">Họ Tên</p>
              <input
                type="text"
                value={formData.Name}
                className="border col-span-2  bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                required
                name="Name"
                onChange={handleInputChange}
              />
            </div>

            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold ">Số điện thoại</p>
              <input
                type="text"
                className="border col-span-2  bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                value={formData.Phone}
                name="Phone"
                onChange={handleInputChange}
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold ">CMND</p>
              <input
                type="text"
                name="Cmnd"
                className="border  col-span-2 bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                value={formData.Cmnd}
                onChange={handleInputChange}
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold ">Email</p>
              <input
                type="text"
                name="Email"
                className="border  col-span-2 bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                value={formData.Email}
                onChange={handleInputChange}
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center  w-full">
              <p className="col-span-1 font-semibold ">Loại khách hàng</p>
              <select
                className="col-span-2 text-center bg-white border border-gray-300 text-gray-900  rounded-md  block w-full py-1"
                value={formData.TypeGuestID}
                name="TypeGuestID"
                onChange={handleInputChange}
              >
                {optionsTypeGuest?.map((option: any) => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </select>
            </div>
            <button
              type="submit"
              className=" font-semibold border-black border-[1px] rounded-md px-2 py-1 w-full hover:bg-slate-200 duration-300 active:bg-slate-500 "
            >
              Cập nhật
            </button>
          </div>
        </form>
      </Modal>
    </div>
  );
};

export default page;
