import { EditableTableForm } from "@/components/form/formHireRoom/EditableTableForm";
import React from "react";

const HireRoomPage = () => {
  return (
    <>
      <EditableTableForm />
    </>
  );
};

export default HireRoomPage;
