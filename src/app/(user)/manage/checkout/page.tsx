"use client";
import { SearchProps } from "antd/es/input";
import React, { useEffect, useState } from "react";
import { Button, Input, Table, Space, SelectProps } from "antd";

import { jsPDF } from "jspdf";
import { quanLyDatPhongServices, quanLyRoomServices } from "@/services";
import { FaPrint } from "react-icons/fa";
import { IoCheckboxOutline } from "react-icons/io5";
import { toast } from "react-toastify";
const { Search } = Input;

const checkoutPage = () => {
  const [searchGuest, setSearchGuest] = useState("");
  const [dataGuest, setGuestData] = useState([]);

  const columns: any = [
    {
      title: "ID Đặt Phòng",
      dataIndex: "BookingID",
      key: "BookingID",
      align: "center",
    },
    {
      title: "Phòng ",
      dataIndex: "RoomID",
      key: "RoomID",
      align: "center",
    },
    {
      title: "Tên Khách hàng",
      dataIndex: "BookingGuest",
      key: "BookingGuest",
      align: "center",
      render: (guests: any) => (
        <div>
          {guests &&
            guests.map((guest: any) => (
              <div key={guest.Guest.GuestID}>{guest.Guest.Name}</div>
            ))}
        </div>
      ),
    },
    {
      title: "Ngày Nhận Phòng",
      dataIndex: "CheckinDate",
      key: "CheckinDate",
      align: "center",
      render: (date: any) => new Date(date).toLocaleDateString("vi-VN"),
    },
    {
      title: "Ngày Trả Phòng",
      dataIndex: "CheckoutDate",
      key: "CheckoutDate",
      align: "center",
      render: (date: any) => new Date(date).toLocaleDateString("vi-VN") ,
    },
    {
      title: "Hiện Trạng",
      dataIndex: "IsPaid",
      key: "IsPaid",
      align: "center",
      render: (revenue: any) => (
        <>
          {revenue === false ? (
            <span>Chưa thanh toán </span>
          ) : (
            <span>Đã thanh toán </span>
          )}
        </>
      ),
    },
    {
      title: "Thanh Toán",
      dataIndex: "TotalPrice",
      key: "TotalPrice",
      align: "center",
      render: (revenue: any) => (
        <span>
          {new Intl.NumberFormat("vi-VN", {
            style: "currency",
            currency: "VND",
          }).format(revenue)}
        </span>
      ),
    },

    {
      title: "Thao Tác",
      key: "action",
      align: "center",
      render: (_: any, record: any) => (
        <Space size="middle">
          <Button
            type="default"
            danger
            onClick={async () => {
              try {
                await quanLyRoomServices.updateRoom(record.RoomID, {
                  Status: true,
                });
                await quanLyDatPhongServices.updateBookingByUserID(
                  record.BookingID,
                  {
                    IsPaid: true,
                  }
                );
                toast.success("Checkout thành Công!");
                fetchData();
              } catch (error) {}
            }}
          >
            <IoCheckboxOutline className="text-2xl" />
          </Button>
          <Button type="dashed" danger onClick={() => generatePDF(record)}>
            <FaPrint className="text-2xl" />
          </Button>
        </Space>
      ),
    },
  ];

  const fetchData = async () => {
    try {
      const { data } = searchGuest
        ? await quanLyDatPhongServices.getBookingByUserName(searchGuest)
        : await quanLyDatPhongServices.getBooking();
      //@ts-ignore
      const newDataGuest = data.content.map((item) => ({
        ...item,
        key: item.BookingID,
      }));
      //@ts-ignore
      console.log(data.content);

      setGuestData(newDataGuest);
    } catch (error) {
      console.error("Error fetching guest data:", error);
    }
  };

  useEffect(() => {
    fetchData();
  }, [searchGuest]);

  const onSearch: SearchProps["onSearch"] = async (value, _e, info) => {
    setSearchGuest(value);
    console.log(value);
  };
  function removeAccents(name: any) {
    return name.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }
  const generatePDF = (record: any) => {
    let CheckinDate = new Date(record.CheckinDate).toLocaleDateString();
    let CheckoutDate = new Date(record.CheckoutDate).toLocaleDateString();
    let BookingGuest = record.BookingGuest.map((guest: any) =>
      removeAccents(guest.Guest.Name)
    ).join(", ");
    console.log(BookingGuest);

    var doc = new jsPDF();
    doc.setFontSize(22);
    doc.text("Hoa don", 20, 20);
    doc.setFontSize(16);
    doc.text(`Phong thue: ${record.BookingID.toString()}`, 20, 40);
    doc.text(`Ngày nhan phong: ${CheckinDate}`, 20, 50);
    doc.text(`Ngày tra phong: ${CheckoutDate}`, 20, 60);
    doc.text(`Thanh toan: ${record.TotalPrice.toString()} vnd`, 20, 70);
    doc.text(`Ngui thue: ${BookingGuest}`, 20, 80);
    const MY_BANK = {
      BANK_ID: 970418,
      ACCOUNT_NO: 1351142110,
      TEMPLATE: "compact",
      AMOUNT: record.TotalPrice,
      DESCRIPTION: "CHUYEN TIEN",
      ACCOUNT_NAME: "",
    };

    doc.addImage(
      `https://img.vietqr.io/image/${MY_BANK.BANK_ID}-${MY_BANK.ACCOUNT_NO}-${MY_BANK.TEMPLATE}.png?amount=${MY_BANK.AMOUNT}&addInfo=${MY_BANK.DESCRIPTION}&accountName=${MY_BANK.ACCOUNT_NAME}`,
      "JPEG",
      15,
      100,
      80,
      80
    );

    doc.save("BookingDetails.pdf");
  };

  useEffect(() => {
    fetchData();
  }, [searchGuest]);

  return (
    <div>
      <div className=" mb-4">
        <Search
          placeholder="Tìm hóa đơn theo tên Khách Hàng"
          allowClear
          onSearch={onSearch}
          style={{ width: 400 }}
        />
      </div>
      {dataGuest && <Table dataSource={dataGuest} columns={columns} />}
    </div>
  );
};

export default checkoutPage;
