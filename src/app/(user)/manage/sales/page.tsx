"use client";
import React, { useEffect, useState } from "react";
import { Line, Pie } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  ArcElement,
} from "chart.js";
import { DatePicker, Divider, Table } from "antd";
import { quanLyBaoCaoServices } from "@/services/quanLyBaoCao";
import dayjs from "dayjs";

ChartJS.register(ArcElement, Tooltip, Legend);

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

const createDataSet = (label: string) => ({
  label: label,
  data: [],
  backgroundColor: [
    "rgba(255, 99, 132, 0.2)",
    "rgba(54, 162, 235, 0.2)",
    "rgba(255, 206, 86, 0.2)",
    "rgba(75, 192, 192, 0.2)",
    "rgba(153, 102, 255, 0.2)",
    "rgba(255, 159, 64, 0.2)",
  ],
  borderColor: [
    "rgba(255, 99, 132, 1)",
    "rgba(54, 162, 235, 1)",
    "rgba(255, 206, 86, 1)",
    "rgba(75, 192, 192, 1)",
    "rgba(153, 102, 255, 1)",
    "rgba(255, 159, 64, 1)",
  ],
  borderWidth: 1,
});

const columns = [
  { title: "STT", dataIndex: "stt", key: "stt" },
  { title: "Loại Phòng", dataIndex: "roomType", key: "roomType" },
  {
    title: "Doanh Thu",
    dataIndex: "revenue",
    key: "revenue",
    render: (revenue: any) => (
      <span>
        {new Intl.NumberFormat("vi-VN", {
          style: "currency",
          currency: "VND",
        }).format(revenue)}
      </span>
    ),
  },
  {
    title: "Tỷ Lệ",
    dataIndex: "percentage",
    key: "percentage",
    render: (percentage: any) => (
      <span>{percentage ? `${percentage}%` : ""}</span>
    ),
  },
];

const options = {
  responsive: true,
  plugins: {
    legend: { position: "top" as const },
    title: { display: true, text: "Doanh Thu" },
  },
};

const labels = Array.from({ length: 12 }, (_, i) => `Tháng ${i + 1}`);

const SalesPage = () => {
  const [searchRevenue, setSearchRevenue] = useState(dayjs().year());
  const [totalRevenue, setTotalRevenue] = useState({ year: 0, month: 0 });
  const [searchDate, setSearchDate] = useState({
    year: dayjs().year(),
    month: dayjs().month() + 1,
  });
  const [dataRevenue, setDataRevenue] = useState({
    labels,
    datasets: [
      {
        label: `${searchRevenue}`,
        data: Array(12).fill(0),
        borderColor: "rgb(53, 162, 235)",
        backgroundColor: "rgba(53, 162, 235, 0.5)",
      },
    ],
  });
  const [dataPie, setDataPie] = useState({
    labels: [],
    datasets: [createDataSet("Doanh thu")],
  });

  const [dataPieByMonth, setDataPieByMonth] = useState({
    labels: [],
    datasets: [createDataSet("Doanh thu theo tháng")],
  });
  const [dataRevenueByMonth, setDataRevenueByMonth] = useState<any[]>([]);

  const fetchData = async () => {
    try {
      const { data } = await quanLyBaoCaoServices.getRevenueByYear(
        searchRevenue
      );
      //@ts-ignore
      const revenues = data.content.map((item: any) => item.totalRevenue);
      setDataRevenue((prevData: any) => ({
        ...prevData,
        datasets: [{ ...prevData.datasets[0], data: revenues }],
      }));
      setTotalRevenue((prevTotalRevenue: any) => ({
        ...prevTotalRevenue,
        //@ts-ignore
        year: data.content.reduce(
          (acc: any, month: any) => acc + month.totalRevenue,
          0
        ),
      }));
    } catch (error) {
      console.error("Failed to fetch data:", error);
    }
  };

  const fetchDataByRoomType = async () => {
    try {
      const { data } = await quanLyBaoCaoServices.getRevenueByRoomType(
        searchRevenue
      ); //@ts-ignore
      if (data && data.content) {
        //@ts-ignore
        const formattedData = data.content.map((item: any) => ({
          roomType: item.roomType,
          revenue: item.revenue,
        }));
        setDataPie((prevData: any) => ({
          ...prevData,
          labels: formattedData.map((item: any) => item.roomType),
          datasets: [
            {
              ...prevData.datasets[0],
              data: formattedData.map((item: any) => item.revenue),
            },
          ],
        }));
      }
    } catch (error) {
      console.error("Failed to fetch data:", error);
    }
  };

  const fetchDataByRoomTypeByMonth = async (month: number, year: number) => {
    try {
      const { data } = await quanLyBaoCaoServices.getRevenueByMonth(
        month,
        year
      );
      //@ts-ignore
      if (data && data.content) {
        //@ts-ignore
        const formattedData = data.content.map((item: any) => ({
          roomType: item.roomType,
          revenue: item.revenue,
        }));
        setDataPieByMonth((prevData: any) => ({
          ...prevData,
          labels: formattedData.map((item: any) => item.roomType),
          datasets: [
            {
              ...prevData.datasets[0],
              data: formattedData.map((item: any) => item.revenue),
            },
          ],
        }));
        const totalPriceMonth = formattedData.reduce(
          (acc: any, item: any) => acc + item.revenue,
          0
        );
        setTotalRevenue((prevTotalRevenue: any) => ({
          ...prevTotalRevenue,
          month: totalPriceMonth,
        }));
        //@ts-ignore
        const formattedData1 = data.content.map((item: any, index: number) => ({
          key: index,
          stt: index + 1,
          roomType: item.roomType.trim(),
          revenue: item.revenue,
          percentage: ((item.revenue / totalPriceMonth) * 100).toFixed(2),
        }));
        setDataRevenueByMonth(formattedData1);
      }
    } catch (error) {
      console.error("Failed to fetch data:", error);
    }
  };

  const onSearch = (value: string) => {
    const year = dayjs(value).isValid() ? dayjs(value).year() : dayjs().year();
    setSearchRevenue(year);
  };

  const onChangeMonth: any = async (date: any, dateString: string) => {
    const year = dayjs(date).isValid() ? dayjs(date).year() : dayjs().year();
    const month = dayjs(date).isValid()
      ? dayjs(date).month() + 1
      : dayjs().month() + 1;
    setSearchDate({ year, month });
  };

  useEffect(() => {
    fetchData();
    fetchDataByRoomType();
    fetchDataByRoomTypeByMonth(searchDate.month, searchDate.year);
  }, [searchRevenue, searchDate]);

  const optionsPie = {
    responsive: true,
    plugins: {
      tooltip: {
        callbacks: {
          label: (context: any) => {
            const total = context.dataset.data.reduce(
              (acc: any, curr: any) => acc + curr,
              0
            );
            const percentage = ((context.raw / total) * 100).toFixed(2);
            return `${
              context.label
            }: ${context.raw.toLocaleString()} ₫ ${percentage}%`;
          },
        },
      },
      legend: { position: "top" as const },
    },
  };

  return (
    <div className="p-4 bg-white rounded-md ">
      <h2 className="text-2xl font-semibold">Báo cáo doanh thu theo năm</h2>
      <Divider />
      <div className="mt-2">
        <p className="text-[16px] mb-1 text-[#4e4e4e]">
          Nhập năm cần xem doanh thu
        </p>
        <div className="flex items-center gap-5 font-semibold">
          <DatePicker
            onChange={onSearch}
            picker="year"
            //@ts-ignore
            defaultValue={dayjs().startOf("year")}
          />
          <span>
            Tổng doanh thu năm {searchRevenue}:{" "}
            {totalRevenue.year.toLocaleString()} ₫
          </span>
        </div>
        <div className="grid grid-cols-7 items-center">
          <div className="col-span-5">
            <Line options={options} data={dataRevenue} />
          </div>
          <div className="col-span-2">
            <Pie data={dataPie} options={optionsPie} />
          </div>
        </div>
      </div>
      <h2 className="text-2xl mt-4 font-semibold">
        Báo cáo doanh thu theo tháng
      </h2>
      <Divider />
      <div className="mb-2">
        <p className="text-[16px] mb-1 text-[#4e4e4e]">
          Nhập tháng cần xem doanh thu
        </p>
        <div className="flex items-center gap-5 font-semibold">
          <DatePicker
            onChange={onChangeMonth}
            picker="month"
            defaultValue={dayjs().startOf("month")}
          />
          <span>
            Tổng doanh thu tháng {searchDate.year}/{searchDate.month}:{" "}
            {totalRevenue.month.toLocaleString()} vnđ
          </span>
        </div>
        <div className="grid grid-cols-7 items-center">
          <div className="col-span-5">
            <Table columns={columns} dataSource={dataRevenueByMonth} />
          </div>
          <div className="col-span-2">
            <Pie data={dataPieByMonth} options={optionsPie} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default SalesPage;
