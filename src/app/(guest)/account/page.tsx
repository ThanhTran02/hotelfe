"use client";
import QR from "@/components/QR/QR";
import { useAppSelector } from "@/lib/hook";
import { RootState } from "@/lib/store";
import { quanLyDatPhongServices } from "@/services";
import { quanLyNguoiDungServices } from "@/services/quanLyNguoiDung";
import { HomeOutlined, UserOutlined } from "@ant-design/icons";
import { Breadcrumb, Button } from "antd";
import Image from "next/image";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { LuUserCircle2 } from "react-icons/lu";

const page = () => {
  const [infoUser, setInfoUser] = useState<any>();
  const [rooms, setRoom] = useState();
  const { user } = useAppSelector((state: RootState) => state.users);
  console.log(user);

  const fetchDataUser = async () => {
    try {
      if (user) {
        const { data } = await quanLyNguoiDungServices.findGuestfByID(
          //@ts-ignore
          user.GuestID
        );
        console.log(data);
        //@ts-ignore
        setInfoUser(data.content[0]);

        const dataRoom = await quanLyDatPhongServices.getBookingByUserID(
          //@ts-ignore
          user.GuestID
        );
        //@ts-ignore
        console.log(dataRoom.data.content);

        //@ts-ignore
        setRoom(dataRoom.data.content);
      }
    } catch (error) {}
  };

  useEffect(() => {
    fetchDataUser();
  }, []);
  console.log(infoUser);
  console.log(rooms);

  return (
    <div className="w-[1260px] m-auto gap-10 min-h-[700px] py-10">
      <Breadcrumb
        items={[
          {
            title: (
              <Link href={"/"}>
                <HomeOutlined />
                <span>Home</span>
              </Link>
            ),
          },
          {
            title: (
              <>
                <UserOutlined />
                <span>Thông tin cá nhân</span>
              </>
            ),
          },
        ]}
      />
      <div className=" grid grid-cols-3 mt-5">
        <div className="border-r col-span-1 ">
          <h2 className="text-[20px]">Thông tin tài khoản</h2>
          <div className="flex flex-col gap-5 items-start mt-10">
            <LuUserCircle2 className="text-5xl " />
            <p className="cursor-pointer">Chỉnh sửa thôn tin cá nhân</p>
            <p className="font-semibold">
              Tên người dùng:
              <span className="font-normal">{infoUser && infoUser.Name}</span>
            </p>
            <p className="font-semibold">
              Địa chỉ:
              <span className="font-normal">
                {infoUser && infoUser.Address}
              </span>
            </p>
            <p className="font-semibold">
              Số điện thoại:
              <span className="font-normal">{infoUser && infoUser.Phone}</span>
            </p>
            <p className="font-semibold">
              Số cmnd:
              <span className="font-normal">{infoUser && infoUser.Cmnd}</span>
            </p>
          </div>
        </div>

        <div className="px-5 col-span-2">
          <h2 className="text-[20px]">Thông tin đặt phòng</h2>
          <div className="flex gap-10 items-center mt-10">
            <div className="grid grid-cols-1 gap-10  ">
              {rooms &&
                //@ts-ignore
                rooms.map((room: any) => (
                  <div
                    key={room.BookingID}
                    className=" grid grid-cols-4 items-center gap-5"
                  >
                    <Image
                      src={"/img/banner.jpg"}
                      alt=""
                      height={200}
                      width={200}
                    />
                    <div className="">
                      <p className="font-semibold">
                        Phòng:
                        <span className="font-normal">{room.RoomID}</span>
                      </p>
                      <p className="font-semibold">
                        Ngày đặt phòng:
                        <span className="font-normal">
                          {new Date(room.CheckinDate).toLocaleDateString()}
                        </span>
                      </p>
                      <p className="font-semibold">
                        Ngày trả phòng:
                        <span className="font-normal">
                          {" "}
                          {new Date(room.CheckoutDate).toLocaleDateString()}
                        </span>
                      </p>
                      <p className="font-semibold">
                        Thanh toán:
                        <span className="font-normal">
                          {room.TotalPrice}
                          {/* {new Intl.NumberFormat("vi-VN", {
                            style: "currency",
                            currency: "VND",
                          }).format(room.TotalPrice)} */}
                        </span>
                      </p>
                    </div>
                    <div className="col-span-1">
                      <QR amount={room.TotalPrice} room={room.RoomID} />
                    </div>
                    <div className="">
                      <Button danger>Hủy phòng</Button>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default page;
