"use client";
import React, { useEffect } from "react";
import { useRouter } from "next/navigation";

import GuestRoom from "@/components/guest/layout/guest.room";
import { useAuth } from "@/hook";
import "./guest.scss";
const page = () => {
  const { user } = useAuth();
  console.log(user);

  const router = useRouter();
  useEffect(() => {
    //@ts-ignore
    if (user?.role === "admin") {
      router.push("/manage");
    }
  }, [user]);
  return (
    <>
      <section className="banner">
        <div className="w-[1260px] m-auto flex flex-col gap-2 justify-center items-center text-white h-full z-10 relative ">
          <h1 className="text-5xl">Rooms and Suitess</h1>
          <h3 className="text-center text-2xl">
            Tận hưởng các ưu đãi và gói khách sạn <br /> độc quyền tại Thành phố
            Hồ Chí Minh của chúng tôi
          </h3>
        </div>
      </section>
      <section className="w-[1260px] m-auto   py-10 grid grid-cols-2 gap-10 items-center">
        <div className="rounded-md overflow-hidden ">
          <img src="./img/trainghiem.avif" alt="" />
        </div>
        <div>
          <h2 className=" text-4xl font-bold pb-5">Trải nghiệm</h2>
          <p>
            Trải nghiệm Radisson Blu của bạn phải tập trung vào việc cá nhân
            hóa, từng bước một. Bằng cách chú ý đến những chi tiết nhỏ tạo nên
            sự khác biệt lớn, chúng tôi truyền cảm hứng cho những trải nghiệm
            khó quên trong mỗi lần lưu trú. Bước vào những không gian đầy phong
            cách được thiết kế dành cho công việc và giải trí tại một số điểm
            đến được yêu thích trên thế giới. Các chuyên gia ẩm thực của chúng
            tôi cố gắng mang đến nền ẩm thực tiên tiến, đầy phong cách và tinh
            tế cho mọi trải nghiệm ăn uống tại Radisson Blu.
          </p>
        </div>
      </section>
      <section className="w-[1260px] m-auto   py-10">
        <div className="rounded-xl overflow-hidden  grid grid-cols-3  gap-2">
          <div>
            <img src="./img/6.avif" alt="" />
          </div>
          <div>
            <img src="./img/7.avif" alt="" />
          </div>
          <div>
            <img src="./img/1.webp" alt="" className="h-full" />
          </div>
          <div>
            <img src="./img/2.avif" alt="" />
          </div>
          <div>
            <img src="./img/5.avif" alt="" className="h-full" />
          </div>
          <div>
            <img src="./img/3.avif" alt="" />
          </div>
        </div>
      </section>
      <GuestRoom />
    </>
  );
};

export default page;
