"use client";
import {
  Breadcrumb,
  Form,
  Input,
  Radio,
  RadioChangeEvent,
  Rate,
  Select,
} from "antd";
import { HomeOutlined, ProductOutlined } from "@ant-design/icons";
import React, { useEffect, useState } from "react";

import { DatePicker } from "antd";
import { useParams, useRouter } from "next/navigation";
import Link from "next/link";
import { quanLyRoomServices } from "@/services/quanLyRoom";
import { Room } from "@/types";
import ProductNavFor from "@/components/guest/main/room.slick";
import { quanLyDatPhongServices, quanLyQuyDinhServices } from "@/services";
import { toast } from "react-toastify";
import { useAppSelector } from "@/lib/hook";
import { RootState } from "@/lib/store";

const ProductsPage = () => {
  const [form] = Form.useForm();
  const { slug } = useParams();
  const [dataRoom, setDataRoom] = useState<Room | undefined>();
  const [formBooking, setFormBooking] = useState();
  const [value, setValue] = useState(1);
  const [formData, setFormData] = useState([]);
  const [regulation, setRegulation] = useState();
  const router = useRouter();
  const { user } = useAppSelector((state: RootState) => state.users);
  const onChange = (e: RadioChangeEvent) => {
    console.log("radio checked", e.target.value);
    setValue(e.target.value);
  };
  const getRegulation = async () => {
    try {
      const { data } = await quanLyQuyDinhServices.getRegulation();
      console.log(data);
      //@ts-ignore
      setRegulation(data.content);
      // setOptions(newOptions);
    } catch (error) {
      console.log(error);
    }
  };
  const handleChange = (index: number, key: string, value: string) => {
    console.log(`selected ${value} for ${key} of person ${index}`);

    const newFormData = [...formData]; // Tạo một bản sao mới của formData
    if (!newFormData[index]) {
      //@ts-ignore
      newFormData[index] = {};
    }
    //@ts-ignore
    newFormData[index][key] = value;
    setFormData(newFormData);
  };
  const renderInputs = () => {
    const inputs = [];
    for (let i = 0; i < value; i++) {
      inputs.push(
        <div key={i} className="flex py-2 gap-5">
          <Input
            placeholder={`Nhập Họ tên của người ${i + 1}`}
            onChange={(e) => handleChange(i, "Name", e.target.value)}
          />
          <Input
            placeholder={`Nhập cmnd của người ${i + 1}`}
            onChange={(e) => handleChange(i, "Cmnd", e.target.value)}
          />
          <Select
            style={{ width: 200 }}
            placeholder="Quốc tịch"
            onChange={(value) => handleChange(i, "TypeGuestID", value)}
          >
            <Select.Option value={1}>Nội địa</Select.Option>
            <Select.Option value={2}>Nước ngoài</Select.Option>
          </Select>
        </div>
      );
    }
    return inputs;
  };
  const featchRoom = async (id: any) => {
    try {
      const { data } = await quanLyRoomServices.getRoomByID(id);
      console.log(data);

      //@ts-ignore
      setDataRoom(data.content);
      //@ts-ignore
      setFormBooking(data.content);
    } catch (error) {}
  };
  console.log(dataRoom);
  const onFinish = async (values: any) => {
    try {
      const { Date, ...rest } = values;
      const [start, end] = Date;
      const dateHiring = end.diff(start, "days");
      console.log("value", values);
      //@ts-ignore
      const dataForm = {
        //@ts-ignore
        ...formBooking,
        CheckinDate: Date[0],
        CheckoutDate: Date[1],
        users: formData,
      };
      //@ts-ignore
      let price = dataRoom.RoomType.PricePerDay * 1;

      const totalGuests = formData.length;

      if (totalGuests > 2 && regulation) {
        //@ts-ignore
        price += price * regulation[0].Value * 1;
      }
      //@ts-ignore
      const hasForeignGuest = dataForm.users.some(
        (user: any) => user.TypeGuestID == 2
      );
      if (hasForeignGuest && regulation) {
        //@ts-ignore
        price *= regulation[1].Value * 1;
      }

      const totalPrice = price * dateHiring;
      console.log(totalPrice);

      try {
        const { data } = await quanLyDatPhongServices.createBooking({
          ...dataForm,
          TotalPrice: totalPrice,
          //@ts-ignore
          GuestID: user.GuestID,
        });

        await quanLyRoomServices.updateRoom(dataForm.RoomID, { Status: false });
        toast.success("Đặt phòng thành công !");
        form.resetFields();
        router.push("/account");
      } catch (error) {
        console.error("Error creating booking:", error);
        toast.error("Vui lòng kiểm tra lại thông tin !");
      }
    } catch (error) {
      console.error("Error getting room data:", error);
      toast.error("Vui lòng kiểm tra lại thông tin !");
    }
  };
  useEffect(() => {
    featchRoom(slug);
    getRegulation();
  }, []);
  return (
    <div className="mt-5 m-auto w-[1280px] min-h-[400px]">
      <Breadcrumb
        items={[
          {
            title: (
              <Link href={"/"}>
                <HomeOutlined />
                <span>Home</span>
              </Link>
            ),
          },
          {
            title: (
              <>
                <ProductOutlined />
                <span>Booking</span>
              </>
            ),
          },
        ]}
      />
      <div className=" grid grid-cols-5 gap-10 mb-5 items-center min-h-[600px]">
        <div className="col-span-2">
          <ProductNavFor />
        </div>
        <div className="col-span-3">
          <h4 className="font-semibold text-xl">
            TÊN PHÒNG: <>{dataRoom?.RoomID}</>
          </h4>
          <Rate disabled defaultValue={5} />
          <div className=" grid grid-cols-2 py-3 ">
            <p className="font-semibold text-[16px]">
              Tình trạng:
              <span className="font-normal">
                {dataRoom?.Status === true ? "Còn phòng" : "Hết phòng"}
              </span>
            </p>

            <p className="font-semibold text-[16px]">
              Loại phòng:
              <span className="font-normal">{dataRoom?.RoomType.Name}</span>
            </p>
          </div>
          <div className="py-3 border-y-[1px] border-[#dee2e6]">
            <p className="text-[16px] text-red-400 font-semibold">
              Giá phòng mỗi đêm:{dataRoom?.RoomType.PricePerDay} đ
            </p>

            <Form
              name="basic"
              form={form}
              onFinish={onFinish}
              initialValues={{ memver: 1 }}
            >
              <Form.Item name="member">
                <Radio.Group onChange={onChange} value={value}>
                  {/* <Radio.Group> */}
                  <Radio value={1}>1</Radio>
                  <Radio value={2}>2</Radio>
                  <Radio value={3}>3</Radio>
                </Radio.Group>
              </Form.Item>

              {renderInputs()}
              <Form.Item label="Ngày Thuê" name={"Date"}>
                <DatePicker.RangePicker
                  format={"DD-MM-YYYY"}
                  placeholder={["Ngày thuê ", "Ngày trả phòng"]}
                />
              </Form.Item>

              <Form.Item>
                <button
                  type="submit"
                  className=" w-full py-[10px] bg-red-400 duration-500 active:bg-red-600 font-semibold text-white rounded-lg"
                >
                  Đặt phòng ngay
                </button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ProductsPage;
