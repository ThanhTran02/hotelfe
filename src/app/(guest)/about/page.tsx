"use client";
import { quanLyQuyDinhServices } from "@/services";
import React, { useEffect, useState } from "react";

const aboutPage = () => {
  const [dataRegulation, setDataRegulation] = useState([]);
  const fetchData = async () => {
    try {
      const { data } = await quanLyQuyDinhServices.getRegulation();
      // @ts-ignore
      setDataRegulation(data.content);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="my-9 m-auto w-[1280px]  min-h-[500px] ">
      <h1 className="text-2xl pb-2">Quy định khách sạn</h1>

      <h2 className="text-xl">1. Quy định về đặt phòng</h2>
      <ul className="ml-10">
        <li>
          <strong>Thời gian nhận phòng:</strong> 14:00
        </li>
        <li>
          <strong>Thời gian trả phòng:</strong> 12:00
        </li>
        <li>
          <strong>Chính sách hủy phòng:</strong> Hủy trước 48 giờ miễn phí. Sau
          48 giờ, phí hủy phòng là 50% giá trị đặt phòng.
        </li>
        <li>
          <strong>Đặt cọc:</strong> Yêu cầu đặt cọc bằng tiền mặt hoặc thẻ tín
          dụng khi nhận phòng.
        </li>
      </ul>

      <h2 className="text-xl">2. Quy định về thanh toán</h2>
      <ul className="ml-10">
        <li>
          <strong>Phương thức thanh toán:</strong> Chấp nhận tiền mặt, thẻ tín
          dụng, thẻ ghi nợ và các phương thức thanh toán điện tử.
        </li>
        <li>
          <strong>Thời gian thanh toán:</strong> Phải thanh toán toàn bộ chi phí
          trước khi nhận phòng hoặc khi sử dụng dịch vụ phát sinh.
        </li>
      </ul>

      <h2 className="text-xl">3. Quy định về hành lý và tài sản cá nhân</h2>
      <ul className="ml-10">
        <li>
          <strong>Bảo quản hành lý:</strong> Khách sạn không chịu trách nhiệm
          đối với tài sản cá nhân bị mất hoặc hư hỏng nếu không gửi tại quầy lễ
          tân hoặc tủ an toàn.
        </li>
        <li>
          <strong>Tài sản giá trị:</strong> Khách nên gửi tài sản có giá trị vào
          két an toàn tại phòng hoặc quầy lễ tân.
        </li>
      </ul>

      <h2 className="text-xl">4. Quy định về sử dụng dịch vụ</h2>
      <ul className="ml-10">
        <li>
          <strong>Giờ mở cửa nhà hàng:</strong> 06:00 - 22:00
        </li>
        <li>
          <strong>Dịch vụ phòng:</strong> Có sẵn từ 06:00 - 24:00
        </li>
        <li>
          <strong>Bể bơi:</strong> Mở cửa từ 06:00 - 20:00. Không được mang đồ
          ăn, uống vào khu vực bể bơi.
        </li>
      </ul>

      <h2 className="text-xl">5. Quy định về Phụ phí </h2>
      <ul className="ml-10">
        {dataRegulation &&
          dataRegulation?.map((item: any) => (
            <li key={item.RegulationID}>
              <strong>{item.Description}:</strong> {item.Value}
            </li>
          ))}
      </ul>
    </div>
  );
};

export default aboutPage;
