import FooterGuestPage from "@/components/guest/footer/footer.guest";
import HeaderGuest from "@/components/guest/header/header.guest";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <>
      <HeaderGuest />
      {children}
      <FooterGuestPage />
    </>
  );
}
