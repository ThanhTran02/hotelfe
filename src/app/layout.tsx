import React from "react";

import { AntdRegistry } from "@ant-design/nextjs-registry";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";

import StoreProvider from "./StoreProvider";

import "./globals.css";

const RootLayout = ({ children }: React.PropsWithChildren) => (
  <html lang="en">
    <body>
      <StoreProvider>
        <ToastContainer autoClose={2000} />
        <AntdRegistry>{children}</AntdRegistry>
      </StoreProvider>
    </body>
  </html>
);
export default RootLayout;
