"use client";
import { UserOutlined } from "@ant-design/icons";
import FormSignIn from "@/components/auth/signin/form.signin";
import { useAuth } from "@/hook";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
const SignInPage = () => {
  const { user } = useAuth();
  const router = useRouter();
  useEffect(() => {
    //@ts-ignore
    if (user?.role === "admin") {
      router.push("/manage");
    }
    //@ts-ignore
    if (user?.role === "user") {
      router.push("/");
    }
  }, [user]);
  return (
    <div className="bg-[url(https://images.rawpixel.com/image_800/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvdjU0NmJhdGNoMy1teW50LTM0LWJhZGdld2F0ZXJjb2xvcl8xLmpwZw.jpg)] bg-center bg-cover h-screen w-screen justify-center items-center flex flex-col">
      <div className="grid grid-cols-5 gap-5 w-3/5 bg-white rounded-xl overflow-hidden p-4 h-[550px]">
        <div className="col-span-3  bg-[url(/img/48.jpg)] bg-center bg-cover rounded-xl"></div>
        <div className="flex flex-col gap-5  col-span-2 shadow-3xl rounded-xl px-3 py-4 ">
          <div className="flex flex-col justify-center items-center">
            <UserOutlined className="h-10" />
            <h2 className="m-0 text-center">Đăng nhập </h2>
          </div>
          <div className="">
            <FormSignIn />
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignInPage;
