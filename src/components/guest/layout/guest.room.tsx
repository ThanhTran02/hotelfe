"use client";
import React, { useEffect, useState } from "react";
import CartRoom from "../main/cart.guest";
import { quanLyRoomServices } from "@/services/quanLyRoom";

const GuestRoom = () => {
  const array = new Array(8).fill(0);
  const [rooms, setRooms] = useState<any>();
  const featchRoom = async () => {
    try {
      const data = await quanLyRoomServices.getRoom();
      // console.log(data);
      //@ts-ignore
      setRooms(data.data.content);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    featchRoom();
  }, []);
  return (
    <section className="w-[1260px] m-auto flex flex-col gap-2 justify-center h-full z-10 relative py-5 ">
      <h2 className="text-4xl font-bold pb-5 ">Danh Sách Phòng</h2>
      <div className="grid grid-cols-3 gap-7  text-white">
        {rooms &&
          rooms.map((room: any) => (
            <div key={room.RoomID}>
              <CartRoom room={room} />
            </div>
          ))}
      </div>
    </section>
  );
};

export default GuestRoom;
