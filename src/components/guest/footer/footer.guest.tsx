"use client";
import React from "react";

const FooterGuestPage = () => {
  return (
    <footer className="bg-[#29343d] ">
      <div className="w-[1280px] m-auto flex items-center justify-between gap-10 text-white py-5">
        <div>
          <ul>
            <li>Khách sạn Eastin Grand Sài Gòn</li>
            <li>253 Nguyễn Văn Trỗi, Phường 10,</li>
            <li>Quận Phú Nhuận, Thành phố Hồ Chí Minh, Việt Nam</li>
            <li>ĐT: +84 2838 449 222</li>
            <li>Email: rsvn@eastinggrandsaigon.com</li>
          </ul>
        </div>
        <div>
          <h3 className="text-xl mb-2">Quick links</h3>
          <ul>
            <li>Room booking</li>
            <li>Rooms</li>
            <li>Contact</li>
            <li>Explore</li>
          </ul>
        </div>
        <div>
          <h3 className="text-xl mb-2">Company</h3>
          <ul>
            <li>Privacy policy</li>
            <li>Refund policy</li>
            <li>F.A.Q</li>
            <li>About</li>
          </ul>
        </div>
        <div>
          <h3 className="text-xl mb-2">Social media</h3>
          <ul>
            <li>Facebook</li>
            <li>Twitter</li>
            <li>instagram</li>
            <li>LinkedIn</li>
          </ul>
        </div>
      </div>
      <div className=" text-white py-1 = border-t-solid border-t-[1px]  border-white ">
        <p className="text-center ">Copyright © 2024 </p>
      </div>
    </footer>
  );
};

export default FooterGuestPage;
