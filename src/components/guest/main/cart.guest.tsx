"use client";
import React from "react";
import { CiWifiOn } from "react-icons/ci";
import { PiShower, PiTelevisionSimpleThin } from "react-icons/pi";
import "./cart.guest.scss";
import { useRouter } from "next/navigation";
import { Room } from "@/types";

const CartRoom = ({ room }: { room: Room }) => {
  const router = useRouter();
  return (
    <div className="text-[#7C6A46] card-room  cursor-pointer overflow-hidden  ">
      <img src={"/img/banner.jpg"} alt="" className="w-full"></img>
      <div className="px-4 pb-4 flex flex-col gap-2 bg-white py-5 ">
        <div className="flex justify-between items-center">
          <p className="text-xl font-semibold p-0 m-0">{room.RoomType.Name} </p>
          <p>Tình trạng: {room.Status === true ? "Còn phòng" : "Hết phòng"}</p>
        </div>
        <p className="m-0 p-0 text-[16px]">
          Giá phòng:{room.RoomType.PricePerDay}đ
        </p>
        <div className="flex justify-between items-center">
          <div className="flex items-center gap-2 text-xl">
            <PiTelevisionSimpleThin />
            <PiShower />
            <CiWifiOn />
          </div>
          <button
            className={`status ${
              room.Status ? "cursor-pointer" : "cursor-not-allowed"
            } py-1 px-5  outline-none  border-black border-[1px] text-[16px] text-black duration-300 hover:bg-black hover:text-white `}
            onClick={() =>
              room.Status && router.push(`/booking/${room.RoomID}`)
            }
          >
            Đặt phòng ngay
          </button>
        </div>
      </div>
    </div>
  );
};

export default CartRoom;
