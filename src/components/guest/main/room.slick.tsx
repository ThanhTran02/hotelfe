"use client";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { SlArrowLeft, SlArrowRight } from "react-icons/sl";
import "./slick.scss";
import { Button } from "antd";
const NextArrow = (props: any) => {
  return (
    <Button
      color="inherit"
      onClick={props.onClick}
      style={{
        position: "absolute",
        right: 0,
        top: "45%",
        zIndex: 2,
        width: 30,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <SlArrowRight />
    </Button>
  );
};

const PrevArrow = (props: any) => {
  return (
    <Button
      color="inherit"
      onClick={props.onClick}
      style={{
        position: "absolute",
        top: "45%",
        zIndex: 2,
        minWidth: 30,
        width: 30,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <SlArrowLeft />
    </Button>
  );
};
function ProductNavFor() {
  const settings = {
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };
  return (
    <div className="slider-container">
      <Slider {...settings}>
        <div className="">
          <img src="/img/banner.jpg" className="w-[400px] m-auto" />
        </div>
        <div className="">
          <img src="/img/banner.jpg" className="w-[400px] m-auto" />
        </div>
        <div className="">
          <img src="/img/banner.jpg" className="w-[400px] m-auto" />
        </div>
        <div className="">
          <img src="/img/banner.jpg" className="w-[400px] m-auto" />
        </div>
      </Slider>
    </div>
  );
}

export default ProductNavFor;
