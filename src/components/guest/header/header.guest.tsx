"use client";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { usePathname } from "next/navigation";

import { Popover } from "antd";
import { RxAvatar } from "react-icons/rx";
import { useAuth } from "@/hook";
import { useAppDispatch } from "@/lib/hook";
import { userAction } from "@/lib/features/userSlice";

import "./header.guest.scss";

const HeaderGuest = () => {
  const pathname = usePathname();
  const [isHeaderVisible, setHeaderVisible] = useState(true);
  const { user } = useAuth();

  const dispatch = useAppDispatch();
  const content = (
    <div className=" flex flex-col gap-2">
      <Link href={"/account"}>Thông tin tài khoản</Link>
      <Link
        href={"/"}
        onClick={() => {
          dispatch(userAction.logOut());
        }}
      >
        Đăng xuất
      </Link>
    </div>
  );

  useEffect(() => {
    const handleScroll = () => {
      const currentScrollPos = window.pageYOffset;
      const isVisible = currentScrollPos < prevScrollPos;
      setHeaderVisible(isVisible);
      prevScrollPos = currentScrollPos;
    };

    let prevScrollPos = window.pageYOffset;

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  return (
    <header
      className={
        isHeaderVisible
          ? "header_scrolled_not_active"
          : "header_scrolled_active"
      }
    >
      <div className="w-[1280px] grid grid-cols-4 items-center py-5 m-auto">
        <div className="col-span-1">
          <Link href={"/"} className="cursor-pointer">
            {" "}
            <img src="./img/logo.svg" alt="" className="w-[150px]" />
          </Link>
        </div>
        <div className="flex justify-end   items-center gap-10 col-span-3">
          <ul className="flex justify-center items-center gap-10">
            <li className={`link ${pathname === "/" ? "active" : ""}`}>
              <Link href={"/"} className="font-semibold text-lg">
                Trang chủ
              </Link>
            </li>
            <li className={`link ${pathname === "/rooms" ? "active" : ""}`}>
              <Link href={"/rooms"} className="text-lg font-semibold">
                Danh sách phòng
              </Link>
            </li>
            <li className={`link ${pathname === "/about" ? "active" : ""}`}>
              <Link href={"/about"} className="text-lg font-semibold">
                Chính sách
              </Link>
            </li>
          </ul>
          <div className=" flex item-center gap-2">
            {user !== null ? (
              <div className=" cursor-pointer">
                <Popover content={content} trigger="hover">
                  <button>
                    <RxAvatar className="text-2xl" />
                  </button>
                </Popover>
              </div>
            ) : (
              <>
                <Link href={"/auth/signin"}>
                  <button className="border-black border-[1px] text-[16px] font-semibold px-2 py-1 rounded-md">
                    Đăng nhập
                  </button>
                </Link>
                <Link href={"/auth/signin"}>
                  <button className="border-black border-[1px] border-dashed text-[16px] font-semibold px-2 py-1 rounded-md">
                    Đăng ký
                  </button>
                </Link>
              </>
            )}
          </div>
        </div>
      </div>
    </header>
  );
};

export default HeaderGuest;
