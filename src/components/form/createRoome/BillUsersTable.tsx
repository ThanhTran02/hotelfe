"use client";
import * as React from "react";
import { Form, Input, Button, Table } from "antd";
import { PlusOutlined, EditOutlined, MinusOutlined } from "@ant-design/icons";

const { Column } = Table;

export const BillUsersTable = (props: any) => {
  const { room, add, remove } = props;
  const usersWithSTT = room.map((item: any, index: any) => ({
    ...item,
    stt: index + 1,
    thanhTien: item.soNgayThue * item.donGia,
  }));
  console.log(usersWithSTT);

  return (
    <Table
      dataSource={usersWithSTT}
      pagination={false}
      footer={() => {
        return (
          <Form.Item>
            <Button onClick={add}>
              <PlusOutlined /> Add
            </Button>
          </Form.Item>
        );
      }}
    >
      <Column title="STT" dataIndex="stt" key="stt" />
      <Column
        dataIndex={"phong"}
        title={"Phòng"}
        render={(value, row, index) => {
          return (
            <Form.Item name={[index, "name"]}>
              <Input
                placeholder="name"
                style={{ width: "50%", marginRight: 8 }}
                className="translate-y-1/2"
              />
            </Form.Item>
          );
        }}
      />
      <Column
        dataIndex={"soNgayThue"}
        title={"Số Ngày Thuê"}
        render={(value, row, index) => {
          return (
            <Form.Item name={[index, "customerType"]}>
              <Input
                placeholder="1"
                style={{ width: "50%", marginRight: 8 }}
                className="translate-y-1/2"
              />
            </Form.Item>
          );
        }}
      />
      <Column
        dataIndex={"donGia"}
        title={"Đơn Giá"}
        render={(value, row, index) => {
          return (
            <Form.Item name={[index, "donGia"]}>
              <Input
                placeholder="donGia"
                style={{ width: "50%", marginRight: 8 }}
                className="translate-y-1/2"
              />
            </Form.Item>
          );
        }}
      />
      <Column
        dataIndex={"thanhTien"}
        title={"Thành Tiền"}
        render={(value, row, index) => {
          return (
            <Form.Item name={[index, "price"]}>
              <Input
                //@ts-ignore
                value={row.thanhTien}
                placeholder="vnd"
                style={{ width: "50%", marginRight: 8 }}
                className="translate-y-1/2"
              />
            </Form.Item>
          );
        }}
      />
      <Column
        title={"Action"}
        render={(value, row: any, index) => {
          return (
            <>
              <Button
                icon={<MinusOutlined />}
                shape={"circle"}
                onClick={() => remove(row.name)}
              />
            </>
          );
        }}
      />
    </Table>
  );
};
