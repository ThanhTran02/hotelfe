"use client";
import * as React from "react";
import { Form, Button, Input, DatePicker } from "antd";
import { BillUsersTable } from "./BillUsersTable";
import { BillRoomType, UserType } from "@/types";
interface MocDataType {
  users: string;
  room: BillRoomType[];
  adress: string;
  price: string;
}
const mockData: MocDataType = {
  users: "",
  room: [{ phong: "", soNgayThue: "", donGia: "", thanhTien: "0vnd" }],
  adress: "",
  price: "",
};

export const BillTableForm = () => {
  const onFinish = (values: any) => {
    console.log("Received values of form:", values);
  };

  return (
    <Form name="dynamic_form_item" onFinish={onFinish} initialValues={mockData}>
      <Form.Item name={"name"} label={"Khách Hàng"}>
        <Input placeholder="name" style={{ width: "25%", marginRight: 8 }} />
      </Form.Item>
      <div className="flex justify-between px-10">
        <Form.Item name={"adress"} label={"Địa chỉ"}>
          <Input
            placeholder="adress"
            style={{ width: "50%", marginRight: 8 }}
          />
        </Form.Item>
        <Form.Item label="Giá trị" name={"bill"}>
          <Input placeholder="price" style={{ width: "50%", marginRight: 8 }} />
        </Form.Item>
      </div>

      <Form.List name="room">
        {(room, { add, remove }) => {
          return <BillUsersTable room={room} add={add} remove={remove} />;
        }}
      </Form.List>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};
