"use client";
import { useEffect, useState } from "react";

import { Form, Button, Input, DatePicker, SelectProps, Select } from "antd";
import { toast } from "react-toastify";

import { EditableUsersTable } from "./EditableUsersTable";
import { UserType } from "@/types";
import { quanLyRoomServices } from "@/services/quanLyRoom";
import { quanLyDatPhongServices } from "@/services/quanLyDatPhong";
import moment from "moment";
import { quanLyQuyDinhServices } from "@/services";
import { useRouter } from "next/navigation";
interface MocDataType {
  RoomID: string;
  users: UserType[];
  Date: any;
}
const mockData: MocDataType = {
  RoomID: "Chọn phòng",
  users: [
    { Name: "", Address: "", Cmnd: "", TypeGuestID: "Chọn Loại khách hàng" },
  ],
  Date: "",
};

export const EditableTableForm = () => {
  const router = useRouter();
  const [options, setOptions] = useState<SelectProps["options"]>([]);
  const [form] = Form.useForm();
  const [regulation, setRegulation] = useState();
  const [maxGuest, setMaxGuest] = useState();
  const [priceRoom, setPriceRoom] = useState(0);
  //
  const onFinish = async (values: MocDataType) => {
    try {
      const { Date, users, RoomID, ...rest } = values;
      const dataForm = {
        ...rest,
        CheckinDate: Date[0],
        CheckoutDate: Date[1],
        users,
        RoomID,
      };

      const [start, end] = Date;
      const dateHiring = end.diff(start, "days");

      const { data } = await quanLyRoomServices.getRoomByID(RoomID);
      //@ts-ignore
      console.log(data);
      //@ts-ignore
      let price = data.content.RoomType.PricePerDay * 1;

      const totalGuests = users.length;

      if (totalGuests > 2 && regulation) {
        //@ts-ignore
        price += price * regulation[0].Value * 1;
      }
      //@ts-ignore
      const hasForeignGuest = users.some((user) => user.TypeGuestID === 2);
      if (hasForeignGuest && regulation) {
        //@ts-ignore
        price *= regulation[1].Value * 1;
      }

      const totalPrice = price * dateHiring;

      try {
        await quanLyRoomServices.updateRoom(RoomID, { Status: false });
        const { data } = await quanLyDatPhongServices.createBooking({
          ...dataForm,
          TotalPrice: totalPrice,
        });
        toast.success("Đặt phòng thành công !");
        form.resetFields();
        //@ts-ignore
        router.push(`/manage/checkout/${data.content.BookingID}`);
      } catch (error) {
        console.error("Error creating booking:", error);
        toast.error("Vui lòng kiểm tra lại thông tin !");
      }
    } catch (error) {
      console.error("Error getting room data:", error);
      toast.error("Vui lòng kiểm tra lại thông tin !");
    }
  };

  const getIDRoom = async () => {
    try {
      const { data } = await quanLyRoomServices.getIDRoom();
      console.log(data);
      //@ts-ignore
      const newOptions = data.content.map((id_room) => ({
        value: id_room.RoomID,
        label: id_room.RoomID,
      }));

      setOptions(newOptions);
    } catch (error) {
      console.log(error);
    }
  };

  const getRegulation = async () => {
    try {
      const { data } = await quanLyQuyDinhServices.getRegulation();
      console.log(data);
      //@ts-ignore
      setRegulation(data.content);
      // setOptions(newOptions);
    } catch (error) {
      console.log(error);
    }
  };
  const onChangRoom = async (e: any) => {
    try {
      const { data } = await quanLyRoomServices.getRoomByID(e);

      //@ts-ignore
      setMaxGuest(data.content.RoomType.MaxGuests);
      //@ts-ignore
      setPriceRoom(data.content.RoomType.PricePerDay);
    } catch (error) {}
  };

  useEffect(() => {
    getIDRoom();
    getRegulation();
  }, []);
  return (
    <Form
      name="dynamic_form_item"
      onFinish={onFinish}
      initialValues={mockData}
      form={form}
    >
      <div className="flex justify-between px-10">
        <Form.Item name={"RoomID"} label={"Room"}>
          <Select
            size="middle"
            style={{ width: 200 }}
            options={options}
            onChange={(e) => onChangRoom(e)}
          />
        </Form.Item>
        <Form.Item label={"Số lượng khách tối đa"}>
          <Input value={maxGuest} style={{ width: 100 }} />
        </Form.Item>
        <Form.Item label={"Đơn giá"}>
          <Input
            value={new Intl.NumberFormat("vi-VN", {
              style: "currency",
              currency: "VND",
            }).format(priceRoom)}
          />
        </Form.Item>
        <Form.Item label="Ngày Thuê" name={"Date"}>
          <DatePicker.RangePicker
            format={"DD-MM-YYYY"}
            placeholder={["Ngày thuê ", "Ngày trả phòng"]}
          />
        </Form.Item>
      </div>

      <Form.List name="users">
        {(users, { add, remove }) => {
          return <EditableUsersTable users={users} add={add} remove={remove} />;
        }}
      </Form.List>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};
