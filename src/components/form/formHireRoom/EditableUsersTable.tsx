"use client";
import { Form, Input, Button, Table, SelectProps, Select } from "antd";
import { PlusOutlined, EditOutlined, MinusOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import { quanLyLoaiKhachHangServices } from "@/services/quanLyLoaiKhachHang";

const { Column } = Table;

export const EditableUsersTable = (props: any) => {
  const { users, add, remove } = props;
  const [options, setOptions] = useState<SelectProps["options"]>([]);

  const usersWithSTT = users.map((user: any, index: any) => ({
    ...user,
    stt: index + 1,
  }));
  const getRoomType = async () => {
    try {
      const { data } = await quanLyLoaiKhachHangServices.getAllGuestType();
      //@ts-ignore
      const newOptions = data.content.map((typeGuest) => ({
        value: typeGuest.GuestTypeID,
        label: typeGuest.Description,
      }));

      setOptions(newOptions);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getRoomType();
  }, []);
  return (
    <Table
      dataSource={usersWithSTT}
      pagination={false}
      footer={() => {
        return (
          <Form.Item>
            <Button
              onClick={() =>
                add({
                  Name: "",
                  Address: "",
                  Cmnd: "",
                  TypeGuestID: "Chọn Loại khách hàng",
                })
              }
            >
              <PlusOutlined /> Add
            </Button>
          </Form.Item>
        );
      }}
    >
      <Column title="STT" dataIndex="stt" key="stt" />
      <Column
        dataIndex={"KhachHang"}
        title={"Khách Hàng"}
        render={(value, row, index) => {
          return (
            <Form.Item name={[index, "Name"]}>
              <Input
                placeholder="name"
                style={{ width: "50%", marginRight: 8 }}
                className="translate-y-1/2"
              />
            </Form.Item>
          );
        }}
      />
      <Column
        dataIndex={"loaiKhach"}
        title={"Loại Khách"}
        render={(value, row, index) => {
          return (
            <Form.Item name={[index, "TypeGuestID"]}>
              <Select
                size="middle"
                options={options}
                style={{ width: 200, marginRight: 8 }}
                className="translate-y-1/2"
              />
            </Form.Item>
          );
        }}
      />
      <Column
        dataIndex={"cmnd"}
        title={"CMND"}
        render={(value, row, index) => {
          return (
            <Form.Item name={[index, "Cmnd"]}>
              <Input
                placeholder="cmnd"
                style={{ width: "50%", marginRight: 8 }}
                className="translate-y-1/2"
              />
            </Form.Item>
          );
        }}
      />
      <Column
        dataIndex={"diaChi"}
        title={"Địa chỉ"}
        render={(value, row, index) => {
          return (
            <Form.Item name={[index, "Address"]}>
              <Input
                placeholder="địa chỉ"
                style={{ width: "50%", marginRight: 8 }}
                className="translate-y-1/2"
              />
            </Form.Item>
          );
        }}
      />
      <Column
        title={"Action"}
        render={(value, row: any, index) => {
          return (
            <>
              <Button
                icon={<MinusOutlined />}
                shape={"circle"}
                onClick={() => remove(row.name)}
              />
            </>
          );
        }}
      />
    </Table>
  );
};
