"use client";
import React, { useEffect, useState } from "react";
import { Button, Modal, Select, Space, Table, Tag } from "antd";
import type { SelectProps, TableProps } from "antd";
import { RoomType } from "@/types";
import { CiEdit } from "react-icons/ci";
import { toast } from "react-toastify";
import { quanLyLoaiPhongServices, quanLyRoomServices } from "@/services";

interface DataType {
  key: string;
  name: string;
  age: number;
  address: string;
  tags: string[];
  stt?: number;
  phong?: string;
  loaiPhong?: string;
  donGia?: number;
  ghiChu?: string;
}

const TablePage = (props: any) => {
  const { data } = props;
  const [resetPage, setResetPage] = useState(false);
  const [formDataUpdate, setFormDataUpdate] = useState({
    RoomID: "",
    TypeRoomID: "",
    Status: "",
  });
  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    setFormDataUpdate({
      ...formDataUpdate,
      [name]: value,
    });
  };
  const [isModalUpdateOpen, setIsModalUpdateOpen] = useState(false);

  const columns: TableProps<DataType>["columns"] = [
    {
      title: "Stt",
      key: "stt",
      dataIndex: "stt",
      align: "center",
    },
    {
      title: "Phòng",
      dataIndex: "room",
      key: "room",
      align: "center",
    },
    {
      title: "Loại Phòng",
      dataIndex: "type",
      key: "price",
      align: "center",
    },
    {
      title: "Đơn Giá",
      dataIndex: "price",
      key: "price",
      align: "center",
      render: (price: any) => (
        <span>
          {new Intl.NumberFormat("vi-VN", {
            style: "currency",
            currency: "VND",
          }).format(price)}
        </span>
      ),
    },
    {
      title: "Ghi chú",
      key: "note",
      dataIndex: "note",
      align: "center",
    },
    {
      title: "Tình Trạng",
      key: "status",
      dataIndex: "status",
      align: "center",
      render: (status) => (status ? "Chưa đặt" : "Đặt phòng"),
    },
    {
      title: "Sức Chứa",
      key: "maxGuests",
      dataIndex: "maxGuests",
      align: "center",
    },
    {
      title: "Chỉnh sửa",
      key: "action",
      align: "center",
      //@ts-ignore
      render: (_, record) => (
        <Space size="middle">
          <Button
            type="dashed"
            danger
            onClick={() => {
              showModallUpdate(record);
            }}
          >
            <CiEdit className="text-2xl" />
          </Button>
        </Space>
      ),
    },
  ];

  const [options, setOptions] = useState<SelectProps["options"]>([]);
  const getRoomType = async () => {
    try {
      const { data } = await quanLyLoaiPhongServices.getTypeRoom();
      //@ts-ignore
      const newOptions = data.content.map((typeGuest) => ({
        value: typeGuest.TypeRoomID,
        label: typeGuest.Name,
      }));

      setOptions(newOptions);
    } catch (error) {
      console.log(error);
    }
  };
  const handleCancelUpdate = () => {
    setIsModalUpdateOpen(false);
  };

  const showModallUpdate = (record: any) => {
    console.log(record);

    setFormDataUpdate({
      RoomID: record.room,
      TypeRoomID: record.roomtype,
      Status: record.status,
    });
    setIsModalUpdateOpen(true);
  };

  let dataTable = null;
  if (data) {
    dataTable = data.map((item: any, index: any) => {
      return {
        key: item.RoomID,
        stt: index + 1,
        room: item.RoomID,
        price: item.RoomType.PricePerDay,
        type: item.RoomType.Name,
        note: item.RoomType.Description,
        status: item.Status,
        maxGuests: item.RoomType.MaxGuests,
        roomtype: item.RoomType.TypeRoomID,
      };
    });
  }
  const onFinishUpdate = async () => {
    console.log("Success:", formDataUpdate);
    const { RoomID, ...newFormData } = formDataUpdate;
    // @ts-ignore
    try {
      const res = await quanLyRoomServices.updateRoom(RoomID, {
        ...newFormData,
        TypeRoomID: Number(newFormData.TypeRoomID),
        Status: Boolean(newFormData.Status),
      });
      toast.success("Cập nhật thông tin phòng thành công!");
      console.log(res);
      handleCancelUpdate();
      setResetPage(!resetPage);
      // fetchData();
    } catch (error) {
      console.error("Error create new Staff:", error);
      toast.error("Vui lòng kiểm tra lại thông tin !");
    }
  };
  useEffect(() => {
    getRoomType();
  }, [resetPage]);
  console.log(formDataUpdate);

  return (
    <>
      <Table columns={columns} dataSource={dataTable} />
      <Modal
        title="Sửa thông tin nhân viên"
        open={isModalUpdateOpen}
        onCancel={handleCancelUpdate}
        footer={false}
      >
        <form
          onSubmit={(e) => {
            e.preventDefault();
            onFinishUpdate();
          }}
        >
          <div className="flex items-start flex-col justify-center gap-3 ">
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">ID Phòng:</p>
              <input
                type="text"
                className="border col-span-2 bg-white border-gray-300 text-gray-900  rounded-md  block w-full py-1 text-center "
                required
                value={formDataUpdate.RoomID}
                disabled
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">Loại Phòng</p>

              <select
                className="col-span-2 text-center bg-white border border-gray-300 text-gray-900  rounded-md  block w-full py-1"
                value={formDataUpdate.TypeRoomID}
                name="TypeRoomID"
                onChange={handleInputChange}
              >
                {options &&
                  options.map((item: any) => (
                    <option value={item.value} key={item.value}>
                      {item.label}
                    </option>
                  ))}
              </select>
            </div>
            <div className="grid grid-cols-3 gap-5 items-center  w-full">
              <p className="col-span-1 font-semibold ">Tình Trạng</p>
              <select
                className="col-span-2 text-center bg-white border border-gray-300 text-gray-900  rounded-md  block w-full py-1"
                value={formDataUpdate.Status}
                name="Status"
                onChange={handleInputChange}
              >
                <option value={"true"}>Chưa đặt</option>
                <option value={"false"}>Đã đặt</option>
              </select>
            </div>

            <button
              type="submit"
              className=" font-semibold border-black border-[1px] rounded-md px-2 py-1 w-full hover:bg-slate-200 duration-300 active:bg-slate-500 "
            >
              Cập nhật
            </button>
          </div>
        </form>
      </Modal>
    </>
  );
};

export default TablePage;
