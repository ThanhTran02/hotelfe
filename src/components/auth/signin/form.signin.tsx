"use client";
import { useAuth } from "@/hook";
import { loginThunk } from "@/lib/features/userThunk";
import { useAppDispatch } from "@/lib/hook";
import { Button, Checkbox, Form, Input } from "antd";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";

type FieldType = {
  Email?: string;
  Password?: string;
  remember?: string;
};

const FormSignIn = () => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const onFinish = async (values: any) => {
    console.log("Success:", values);
    dispatch(loginThunk(values))
      .unwrap()
      .then((data) => {
        toast.success("Đăng nhập thành công !");
        // router.push("/");
      })
      .catch((error: any) => {
        toast.error("Tài khoản hoặc mật khẩu không chính xác !");
      });
  };
  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 24 }}
      initialValues={{ remember: true }}
      layout="vertical"
      onFinish={onFinish}
      autoComplete="off"
    >
      <Form.Item<FieldType>
        label="Username"
        name="Email"
        rules={[{ required: true, message: "Vui lòng nhập email !" }]}
      >
        <Input />
      </Form.Item>

      <Form.Item<FieldType>
        label="Password"
        name="Password"
        rules={[{ required: true, message: "Vui lòng nhâp mật khẩu !" }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item<FieldType>
        name="remember"
        valuePropName="checked"
        wrapperCol={{ offset: 8, span: 16 }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item>
        <button
          type="submit"
          className=" w-full  bg-black text-white py-2 font-semibold text-[16px] rounded-md"
        >
          ĐĂNG NHẬP
        </button>
        <button
          type="button"
          onClick={() => router.push("/auth/signup")}
          className=" w-full mt-4 bg-white text-black  py-2 font-semibold text-[16px] rounded-md border-dashed border-black border-[2px]"
        >
          Đăng ký
        </button>
      </Form.Item>
    </Form>
  );
};
export default FormSignIn;
