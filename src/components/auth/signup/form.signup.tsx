"use client";

import { quanLyAuthServices } from "@/services";
import { Button, Checkbox, Form, Input, InputNumber } from "antd";
import Password from "antd/es/input/Password";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";

const FormSignUp = () => {
  const router = useRouter();
  const onFinish = async (values: any) => {
    console.log("Success:", values);
    try {
      const { data } = await quanLyAuthServices.signUp({
        Email: values.Email,
        Password: values.password,
      });
      console.log(data);
      toast.success("Bạn đã đăng ký tài khoản thành công !");
      router.push("/auth/signin");
    } catch (error: any) {
      console.log(error);
      toast.error(error.response.data.message);
    }
  };
  const [form] = Form.useForm();
  return (
    <Form
      form={form}
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 24 }}
      initialValues={{ remember: true }}
      layout="vertical"
      onFinish={onFinish}
      autoComplete="off"
    >
      <Form.Item
        label="Email"
        name="Email"
        rules={[
          {
            required: true,
            message: "Vui lòng nhập email !",
          },
          {
            type: "email",
            message: "Email không hợp lệ !",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Mật khẩu"
        name="password"
        rules={[{ required: true, message: "Vui lòng nhập mật khẩu!" }]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item
        label="Xác nhận lại mật khẩu "
        name="confirm_password"
        dependencies={["password"]}
        rules={[
          {
            required: true,
            message: "Vui lòng xác nhận lại mật khẩu của bạn!",
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue("password") === value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error("Mật khẩu không trùng khớp!"));
            },
          }),
        ]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item
        label="Cmnd"
        name="Cmnd"
        rules={[
          {
            required: true,
            message: "Vui lòng nhập cmnd của bạn!",
          },
          {
            validator: (_, value) => {
              if (!value || /^\d+$/.test(value)) {
                return Promise.resolve();
              }
              return Promise.reject(new Error("Vui lòng chỉ nhập số"));
            },
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        style={{ marginBottom: 8 }}
        name="agreement"
        valuePropName="checked"
        rules={[
          { required: true, message: "Bạn cần đọc và đồng ý với thỏa thuận!" },
        ]}
      >
        <Checkbox>Tôi đã đọc tất cả điều khoản</Checkbox>
      </Form.Item>
      <Form.Item>
        <button
          type="submit"
          className=" w-full  bg-black text-white py-2 font-semibold text-[16px] rounded-md"
        >
          Đăng ký
        </button>
        <button
          type="button"
          onClick={() => router.push("/auth/signin")}
          className=" w-full mt-4 bg-white text-black  py-2 font-semibold text-[16px] rounded-md border-dashed border-black border-[2px]"
        >
          Bạn đã có tài khoản
        </button>
      </Form.Item>
    </Form>
  );
};
export default FormSignUp;
