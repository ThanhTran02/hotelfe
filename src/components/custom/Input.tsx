export const InputD = ({
  type,
  name,
  placeholder,
  error,
  register,
  className,
  disabled,
  label,
  value,
}: any) => {
  return (
    <div className={`relative ${className} items-start`}>
      {label && (
        <p className="mb-2 text-lg font-semibold text-start text-slate-700">
          {label}:
        </p>
      )}
      <input
        value={value}
        type={type}
        disabled={disabled}
        placeholder={placeholder}
        className="border-[1px] border-[#1dbf73] text-[#212529] outline-none rounded-[4px] px-2 py-1 w-[300px] "
        {...register?.(name || "")}
      />

      {error && <p className="text-red-500 mt-2">{error}</p>}
    </div>
  );
};
