import { quanLyAuthServices } from "@/services";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const loginThunk = createAsyncThunk(
  "quanLyAuth/loginThunk",
  async (payload: any, { rejectWithValue }) => {
    try {
      const { data } = await quanLyAuthServices.login(payload);
      //@ts-ignore
      return data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
