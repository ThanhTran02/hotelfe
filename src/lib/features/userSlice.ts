import { createSlice } from "@reduxjs/toolkit";
import { loginThunk } from "./userThunk";
import { jwtDecode } from "jwt-decode";

const initialState = {
  user: null,
};
export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    logOut: (state) => {
      localStorage.removeItem("token");
      state.user = null;
    },
    getUserInfo: (state) => {
      const accessToken = localStorage.getItem("token") || null;
      if (accessToken) state.user = jwtDecode(accessToken);
    },
  },
  extraReducers: (builder) => {
    //@ts-ignore
    builder.addCase(loginThunk.fulfilled, (state, { payload }) => {
      if (payload) localStorage.setItem("token", payload.token);
      //@ts-ignore
      state.user = jwtDecode(payload.token);
    });
  },
});
export const { actions: userAction, reducer: userReducer } = userSlice;
