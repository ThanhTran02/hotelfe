import { configureStore } from "@reduxjs/toolkit";
import { productsReducer } from "./features/productSlice";
import { userAction, userReducer } from "./features/userSlice";

export const makeStore = () => {
  const store = configureStore({
    reducer: {
      products: productsReducer,
      users: userReducer,
    },
    devTools: true,
  });
  // store.dispatch(userAction.getUserInfo());
  return store;
};
export type AppStore = ReturnType<typeof makeStore>;
export type RootState = ReturnType<AppStore["getState"]>;
export type AppDispatch = AppStore["dispatch"];
